import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/login_controller.dart';
import 'package:iehsanamiri/exceptions/app_exceptions.dart';
import 'package:iehsanamiri/mock_services/mock_login_service.dart';
import 'package:iehsanamiri/presentation/widgets/primary_action_button.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/verification_code_form_field.dart';
import 'package:iehsanamiri/services/login_service.dart';

class VerifyOTPWidget extends StatefulWidget {
  const VerifyOTPWidget({
    super.key,
    required this.onDone,
    required this.onBack,
  });

  final Function onDone;
  final Function onBack;

  @override
  State<VerifyOTPWidget> createState() => _VerifyOTPWidgetState();
}

class _VerifyOTPWidgetState extends State<VerifyOTPWidget> {
  TextEditingController verifyController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  late final String requestId;
  late final String phoneNumber;

  @override
  void initState() {
    requestId = Get.find<LoginController>().requestId.toString();
    phoneNumber = Get.find<LoginController>().phoneNumber.toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Text(
            'A verification code is sent to $phoneNumber via SMS ',
            style: const TextStyle(
              color: Colors.white,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: VerificationCodeFormField(
              verifyController: verifyController,
            ),
          ),
          _buildVerifyButton(),
          _buildBackButton(),
        ],
      ),
    );
  }

  Padding _buildBackButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: MaterialButton(
        onPressed: () => widget.onBack(),
        child: const Text(
          'Not the correct number? Go back.',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _buildVerifyButton() {
    return PrimaryActionButton(
      isLoading: isLoading,
      onPressed: () async {
        if (_formKey.currentState!.validate()) {
          setState(() {
            isLoading = true;
          });
          try {
            //todo: replace when server is up
            // await verifyOTPService(verifyController.text, requestId);
            await mockVerifyOTPService(verifyController.text, requestId);
            setState(() {
              isLoading = false;
            });
            widget.onDone();
          } on AppException catch (e) {
            setState(() {
              isLoading = false;
            });
            UtilFunctions.showErrorSnackBar(context, e.message);
          }
        }
      },
      text: 'Verify',
    );
  }
}
