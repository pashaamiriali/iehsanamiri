import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/login_controller.dart';
import 'package:iehsanamiri/exceptions/app_exceptions.dart';
import 'package:iehsanamiri/mock_services/mock_login_service.dart';
import 'package:iehsanamiri/presentation/widgets/phone_number_form_field.dart';
import 'package:iehsanamiri/presentation/widgets/primary_action_button.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/services/login_service.dart';

class LoginWidget extends StatefulWidget {
  const LoginWidget({super.key, required this.onNext});

  final Function() onNext;

  @override
  State<LoginWidget> createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  TextEditingController phoneController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return
        Form(
          key: _formKey,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: PhoneNumberFormField(
                  phoneController: phoneController,
                ),
              ),
              _buildVerifyButton(),
            ],
          ),


    );
  }

  Widget _buildVerifyButton() {
    return PrimaryActionButton(
      isLoading: isLoading,
      onPressed: () async {
        if (_formKey.currentState!.validate()) {
          Get.find<LoginController>().updatePhoneNumber(phoneController.text);

          setState(() {
            isLoading = true;
          });
          try {
            //todo: replace when the server is back
            // String reqId = await sendOTPService(phoneController.text);
            String reqId = await mockSendOTPService(phoneController.text);
            setState(() {
              isLoading = false;
            });
            widget.onNext();
            Get.find<LoginController>().updateRequestId(reqId);
          } on AppException catch (e) {
            setState(() {
              isLoading = false;
            });
            UtilFunctions.showErrorSnackBar(context, e.message);
          }
        }
      },
      text: 'Login',
    );
  }
}
