import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iehsanamiri/mock_services/mock_profile_service.dart';
import 'package:iehsanamiri/presentation/home_screen/home_screen.dart';
import 'package:iehsanamiri/presentation/login_screen/login_widget.dart';
import 'package:iehsanamiri/presentation/login_screen/verify_widget.dart';
import 'package:iehsanamiri/presentation/widgets/app_name_text.dart';
import 'package:iehsanamiri/presentation/widgets/gradient_background.dart';
import 'package:iehsanamiri/services/profile_service.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    PageController pageController = PageController();
    return Scaffold(
      body: Stack(
        children: [
          const GradientBackground(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Expanded(child: Center(child: AppNameText())),
              Expanded(
                flex: 2,
                child: PageView(
                    controller: pageController,
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      LoginWidget(
                        onNext: () {
                          _goToVerify(pageController);
                        },
                      ),
                      _buildVerifyOTPWidget(context, pageController)
                    ]),
              ),
            ],
          ),
        ],
      ),
    );
  }

  VerifyOTPWidget _buildVerifyOTPWidget(
      BuildContext context, PageController pageController) {
    return VerifyOTPWidget(
      onDone: () async {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Logged In!'),
          ),
        );
        //todo: replace when the server is back
        // await getAccountService();
        await mockGetProfileService();
        Get.off(const HomeScreen());
      },
      onBack: () {
        _goToLogin(pageController);
      },
    );
  }

  void _goToVerify(PageController pageController) {
    pageController.nextPage(
        duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
  }

  void _goToLogin(PageController pageController) {
    pageController.previousPage(
        duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
  }
}
