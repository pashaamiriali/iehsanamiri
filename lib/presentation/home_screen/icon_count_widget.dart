import 'package:flutter/material.dart';

class IconCountWidget extends StatelessWidget {
  const IconCountWidget({
    super.key, required this.icon, required this.count,
  });

  final IconData icon;
  final int count;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Icon(icon),
          Text(count.toString()),
        ],
      ),
    );
  }
}
