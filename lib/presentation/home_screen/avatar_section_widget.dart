import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';

class AvatarSectionWidget extends StatelessWidget {
  const AvatarSectionWidget({
    super.key, required this.account,
  });
final Account account;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(account.displayName),
        Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.purple,
                style: account.stat.hasStory
                    ? BorderStyle.solid
                    : BorderStyle.none,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(100),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Stack(
                children: [
                  account.hasAvatar
                      ? Image.network(
                          account.avatarUrl,
                          width: 100,
                        )
                      : const Icon(
                          Icons.person,
                          size: 100,
                        ),
                  account.isVerified
                      ? const Icon(
                          Icons.verified,
                          color: Colors.blue,
                        )
                      : Container(),
                ],
              ),
            )),
        Text(account.username),
        Text(account.bio),
      ],
    );
  }
}
