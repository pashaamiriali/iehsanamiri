import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/presentation/home_screen/icon_count_widget.dart';

class NotificationAreaWidget extends StatelessWidget {
  const NotificationAreaWidget({
    super.key, required this.account,
  });
final Account account;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconCountWidget(
          icon: Icons.thumb_up,
          count: account.notification.like,
        ),
        IconCountWidget(
          icon: Icons.account_box_rounded,
          count: account.notification.follow,
        ),
        IconCountWidget(
          icon: Icons.supervisor_account_rounded,
          count: account.notification.acceptFollow,
        ),
        IconCountWidget(
          icon: Icons.add_comment_sharp,
          count: account.notification.comment,
        ),
        IconCountWidget(
          icon: Icons.recommend,
          count: account.notification.comment,
        ),
      ],
    );
  }
}
