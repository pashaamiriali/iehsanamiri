import 'package:flutter/material.dart';

class ListTileWithEmpty extends StatelessWidget {
  const ListTileWithEmpty({
    super.key, required this.title, required this.text,

  });
  final String title;
  final String text;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(title),
      trailing: Text(
        text.isNotEmpty ? text : 'Empty',
      ),
    );
  }
}

