import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/presentation/home_screen/list_section_widget.dart';
import 'package:iehsanamiri/presentation/home_screen/list_tile_with_empty.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';

class PersonalInfoSectionWidget extends StatelessWidget {
  const PersonalInfoSectionWidget({
    super.key, required this.account,
  });
  final Account account;
  @override
  Widget build(BuildContext context) {
    return ListSectionWidget(
      title: 'Personal Info',
      children: [
        ListTileWithEmpty(
          title: 'Phone',
          text: account.phone,
        ),
        ListTileWithEmpty(
          title: 'Email',
          text: account.email,
        ),
        ListTileWithEmpty(
          title: 'Gender',
          text: account.gender.name,
        ),
        ListTileWithEmpty(
          title: 'Website',
          text: account.websiteUrl,
        ),
        ListTileWithEmpty(
          title: 'Birthday',
          text: UtilFunctions.intToFormattedDate(
            account.birthday.toInt(),
          ),
        ),
        ListTileWithEmpty(
          title: 'Visibility',
          text: account.visibility.name,
        ),
      ],
    );
  }
}
