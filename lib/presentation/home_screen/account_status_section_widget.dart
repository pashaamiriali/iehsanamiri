import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/presentation/home_screen/list_section_widget.dart';
import 'package:iehsanamiri/presentation/home_screen/list_tile_with_empty.dart';

class AccountStatusSectionWidget extends StatelessWidget {
  const AccountStatusSectionWidget({
    super.key, required this.account,
  });
  final Account account;
  @override
  Widget build(BuildContext context) {
    return ListSectionWidget(
      title: "Account Status",
      children: [
        ListTileWithEmpty(
          title: 'Posts',
          text: account.stat.postCount.toString(),
        ),
        ListTileWithEmpty(
          title: 'Followers',
          text: account.stat.followerCount.toInt().toString(),
        ),
        ListTileWithEmpty(
          title: 'Following',
          text: account.stat.followingCount.toInt().toString(),
        ),
      ],
    );
  }
}
