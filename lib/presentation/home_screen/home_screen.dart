import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/account_controller.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/mock_services/mock_logout_service.dart';
import 'package:iehsanamiri/presentation/edit_profile_screen/edit_profile_screen.dart';
import 'package:iehsanamiri/presentation/home_screen/account_status_section_widget.dart';
import 'package:iehsanamiri/presentation/home_screen/avatar_section_widget.dart';
import 'package:iehsanamiri/presentation/home_screen/business_info_section_widget.dart';
import 'package:iehsanamiri/presentation/home_screen/notification_area_widget.dart';
import 'package:iehsanamiri/presentation/home_screen/personal_info_section_widget.dart';
import 'package:iehsanamiri/presentation/login_screen/login_screen.dart';
import 'package:iehsanamiri/services/logout_service.dart';

class HomeScreen extends GetView<AccountController> {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        appBar: _buildAppBar(),
        body: controller.isLoading.isTrue
            ? const Center(child: CircularProgressIndicator())
            : _buildBody(),
      );
    });
  }

  Widget _buildBody() {
    var account = controller.account.value;

    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: _buildNotificationArea(account),
    );
  }

  ListView _buildNotificationArea(Account account) {
    return ListView(
      children: [
        NotificationAreaWidget(account: account),
        _buildAvatarSection(account),
        _buildPersonalInfoSection(account),
        _buildAccountStatsSection(account),
        _buildBusinessSection(account),
      ],
    );
  }

  Widget _buildAvatarSection(Account account) {
    return AvatarSectionWidget(account: account);
  }

  Padding _buildPersonalInfoSection(Account account) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: PersonalInfoSectionWidget(
        account: account,
      ),
    );
  }

  Widget _buildAccountStatsSection(Account account) {
    return AccountStatusSectionWidget(account: account);
  }

  Widget _buildBusinessSection(Account account) {
    return BusinessInfoSectionWidget(account: account);
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: IconButton(
          onPressed: () async {
            //todo: replace when server is up
            // await logoutService();
            await mockLogoutService();

            Get.off(()=>const LoginScreen());
          },
          icon: const Icon(
            Icons.logout,
            semanticLabel: 'Logout',
          )),
      actions: [
        IconButton(
            onPressed: () async {
              Get.to(()=>const EditProfileScreen());
            },
            icon: const Icon(
              Icons.edit,
              semanticLabel: 'Edit Profile',
            ))
      ],
    );
  }
}
