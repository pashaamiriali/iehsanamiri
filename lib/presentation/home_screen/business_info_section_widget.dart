import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/presentation/home_screen/list_section_widget.dart';
import 'package:iehsanamiri/presentation/home_screen/list_tile_with_empty.dart';

class BusinessInfoSectionWidget extends StatelessWidget {
  const BusinessInfoSectionWidget({
    super.key, required this.account,
  });
final Account account;
  @override
  Widget build(BuildContext context) {
    return ListSectionWidget(
      title: "Business Info",
      children: [
        ListTileWithEmpty(
          title: 'Business Phone',
          text: account.business.phone,
        ),
        ListTileWithEmpty(
          title: 'Business Email',
          text: account.business.email,
        ),
        ListTileWithEmpty(
          title: 'Business Category',
          text: account.business.category,
        ),
        ListTileWithEmpty(
          title: 'Business activity status',
          text: account.business.isActive ? "Active" : "Not active",
        ),
      ],
    );
  }
}
