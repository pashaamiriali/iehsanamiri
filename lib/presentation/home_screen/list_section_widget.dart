import 'package:flutter/material.dart';
class ListSectionWidget extends StatelessWidget {
  const ListSectionWidget({
    super.key,
    required this.children,
    required this.title,
  });

  final List<Widget> children;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(
            color: Colors.black12,
            style: BorderStyle.solid,
            width: 0.5,
          )),
      child: Column(
        children: [
          Text(title),
          ...children,
        ],
      ),
    );
  }
}
