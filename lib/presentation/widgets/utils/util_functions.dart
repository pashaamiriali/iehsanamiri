
import 'package:flutter/material.dart';

class UtilFunctions {
  static var phoneNumberValidator = (value) {
    if (value == null ||
        value.isEmpty ||
        value.length < 11 ||
        value.length > 11) {
      return "Your phone number should be 11 digits.";
    } else {
      return null;
    }
  };
  static var verificationCodeValidator = (value) {
    if (value == null ||
        value.isEmpty ||
        value.length < 6 ||
        value.length > 6) {
      return "Verification code should be 6 digits.";
    } else {
      return null;
    }
  };
  static var showErrorSnackBar = (BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    ));
  };
  static var intToFormattedDate = (int data) {
    var date=DateTime.fromMillisecondsSinceEpoch(data);
    return"${date.year}-${date.month}-${date.day}";
  };
  static var nameValidator = (value, String messsage){
    if(value!=null&&value.isNotEmpty){
      if(value.length<8||value.length>50){
        return messsage ;
      }
    }
  };
  static var emailValidator = (value){
    if(value!=null&&value.isNotEmpty){
      if(value.length<8||value.length>50||(!value.contains('@'))){
        return "The email entered is not correct!" ;
      }
    }
  };
  static String? validatePassword(value) {
    if(value==null){
      return 'Please enter password';
    }
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (value.length<8) {
        return 'Enter valid password';
      } else {
        return null;
      }
    }
  }
}
