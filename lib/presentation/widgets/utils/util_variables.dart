import 'package:flutter/material.dart';

class UtilVars {
  static var roundedInputDecoration = InputDecoration(

    fillColor: Colors.white,
    filled: true,
    contentPadding: const EdgeInsets.symmetric(horizontal: 10),
    hintStyle: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(20),
    ),
  );
}
