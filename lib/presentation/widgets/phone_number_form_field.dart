import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_variables.dart';

class PhoneNumberFormField extends StatelessWidget {
  const PhoneNumberFormField({
    super.key,
    required this.phoneController,
  });

  final TextEditingController phoneController;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.number,
      controller: phoneController,
      validator: UtilFunctions.phoneNumberValidator,
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      decoration: UtilVars.roundedInputDecoration.copyWith(hintText: "Enter your phone number"),
    );
  }
}
