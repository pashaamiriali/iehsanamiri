import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_variables.dart';

class VerificationCodeFormField extends StatelessWidget {
  const VerificationCodeFormField({
    super.key,
    required this.verifyController,
  });

  final TextEditingController verifyController;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.number,
      controller: verifyController,
      validator: UtilFunctions.verificationCodeValidator,
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      decoration: UtilVars.roundedInputDecoration
          .copyWith(hintText: "Enter the verification code"),
      textAlign: TextAlign.center,
      style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
    );
  }
}
