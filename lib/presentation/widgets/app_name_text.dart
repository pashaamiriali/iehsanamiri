import 'package:flutter/material.dart';

class AppNameText extends StatelessWidget {
  const AppNameText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      'Iehsanamiri',
      style: TextStyle(
          fontSize: 24,
          color: Colors.white.withOpacity(0.9),
          fontStyle: FontStyle.italic),
    );
  }
}
