import 'package:flutter/material.dart';

class NoneIfEmptyText extends StatelessWidget {
  const NoneIfEmptyText({super.key,required this.txt});

  final String txt;

  @override
  Widget build(BuildContext context) {
    return txt.isEmpty ? Container() : Text(txt);
  }
}
