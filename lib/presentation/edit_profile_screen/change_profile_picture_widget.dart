import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';

class ChangeProfilePictureWidget extends StatelessWidget {
  const ChangeProfilePictureWidget({
    super.key,
    required this.account,
  });

  final Account account;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Center(
            child: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: account.hasAvatar
              ? Image.network(
                  account.avatarUrl,
                  width: 100,
                )
              : const Icon(
                  Icons.person,
                  size: 100,
                ),
        )),
        Positioned(
          bottom: 0,
          child: IconButton(onPressed: () {}, icon: const Icon(Icons.image)),
        )
      ],
    );
  }
}
