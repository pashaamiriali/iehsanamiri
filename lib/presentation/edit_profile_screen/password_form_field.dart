import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_variables.dart';

class PasswordFormField extends StatelessWidget {
  const PasswordFormField({
    super.key,
    required this.passwordController,
    required this.value,
  });

  final TextEditingController passwordController;
  final String value;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: passwordController..text = value,
      validator: UtilFunctions.validatePassword,
      obscureText: true,
      enableSuggestions: false,
      autocorrect: false,
      decoration: UtilVars.roundedInputDecoration
          .copyWith(hintText: "Enter a secure password",label: Text('Password')),
    );
  }
}
