import 'package:flutter/material.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_variables.dart';

class EmailFormField extends StatelessWidget {
  const EmailFormField({
    super.key,
    required this.controller,
    required this.value,
  });

  final TextEditingController controller;
  final String value;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        controller: controller..text = value,
        keyboardType: TextInputType.emailAddress,
        validator: UtilFunctions.emailValidator,
        decoration: UtilVars.roundedInputDecoration
            .copyWith(hintText: "Enter your email address",label: const Text('Email address')));
  }
}
