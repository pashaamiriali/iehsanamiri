import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/account_controller.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/mock_services/mock_profile_service.dart';
import 'package:iehsanamiri/presentation/edit_profile_screen/change_profile_picture_widget.dart';
import 'package:iehsanamiri/presentation/edit_profile_screen/name_form_field.dart';
import 'package:iehsanamiri/presentation/edit_profile_screen/password_form_field.dart';
import 'package:iehsanamiri/presentation/edit_profile_screen/phone_form_field.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_variables.dart';
import 'package:iehsanamiri/services/profile_service.dart';
import 'package:protobuf/protobuf.dart';
import 'package:fixnum/fixnum.dart' as $fixnum;
import 'email_form_field.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({super.key});

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late Account account;
  late TextEditingController usernameController;
  late TextEditingController nameController;
  late TextEditingController phoneController;
  late TextEditingController emailController;
  late TextEditingController passwordController;
  late TextEditingController bioController;
  late TextEditingController websiteController;
  late TextEditingController businessPhoneController;
  late TextEditingController businessEmailController;
  late bool isBusinessAccountActive;
  late GenderTypes selectedGenderType;
  late DateTime birthday;

  late VisibilityTypes selectedVisibilityType;

  @override
  void initState() {
    account = Get.find<AccountController>().account.value;
    usernameController = TextEditingController(text: account.username);
    nameController = TextEditingController(text: account.displayName);
    phoneController = TextEditingController(text: account.phone);
    emailController = TextEditingController(text: account.email);
    passwordController = TextEditingController(text: account.password);
    bioController = TextEditingController(text: account.bio);
    websiteController = TextEditingController(text: account.websiteUrl);
    businessPhoneController =
        TextEditingController(text: account.business.phone);
    businessEmailController =
        TextEditingController(text: account.business.email);
    isBusinessAccountActive = account.business.isActive;
    selectedGenderType = account.gender;
    selectedVisibilityType = account.visibility;
    birthday = DateTime.fromMillisecondsSinceEpoch(account.birthday.toInt());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      bool isLoading = Get.find<AccountController>().isLoading.value;
      return Scaffold(
        appBar: _buildAppBar(),
        body: isLoading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : buildBody(context),
      );
    });
  }

  Form buildBody(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        children: [
          ChangeProfilePictureWidget(account: account),
          NameFormField(
            controller: usernameController,
            value: account.username,
            errorText: "Username should be between 8-50 characters",
            hint: 'Username',
            label: 'Username',
          ),
          const SizedBox(
            height: 12,
          ),
          NameFormField(
            controller: nameController,
            value: account.displayName,
            errorText: "Name should be between 8-50 characters",
            hint: 'Your Name',
            label: 'Display name',
          ),
          const SizedBox(
            height: 12,
          ),
          PhoneFormField(
            controller: phoneController,
            value: account.phone,
          ),
          const SizedBox(
            height: 12,
          ),
          EmailFormField(
            controller: emailController,
            value: account.email,
          ),
          const SizedBox(
            height: 12,
          ),
          PasswordFormField(
            passwordController: passwordController,
            value: account.password,
          ),
          const SizedBox(
            height: 12,
          ),
          DropdownButtonFormField(
            value: account.gender,
            decoration: UtilVars.roundedInputDecoration
                .copyWith(label: const Text('Your Gender')),
            items: List.generate(
              GenderTypes.values.length,
              (index) => DropdownMenuItem(
                value: GenderTypes.values[index],
                enabled: index == 0,
                child: Text(GenderTypes.values[index].name),
              ),
            ),
            onChanged: (val) {
              selectedGenderType = val ?? GenderTypes.None;
            },
          ),
          const SizedBox(
            height: 12,
          ),
          NameFormField(
            label: 'Bio',
            controller: bioController,
            value: account.bio,
            errorText: "Bio should be between 8-50 characters",
            hint: 'Your short bio',
          ),
          const SizedBox(
            height: 12,
          ),
          NameFormField(
            controller: websiteController,
            value: account.websiteUrl,
            errorText: "Website should be between 8-50 characters",
            hint: 'Your website',
            label: 'Website Url',
          ),
          const SizedBox(
            height: 12,
          ),
          buildBirthdayField(context, account),
          const SizedBox(
            height: 12,
          ),
          DropdownButtonFormField(
            value: account.visibility,
            decoration: UtilVars.roundedInputDecoration
                .copyWith(label: const Text('Account visibility')),
            items: List.generate(
              VisibilityTypes.values.length,
              (index) => DropdownMenuItem(
                value: VisibilityTypes.values[index],
                enabled: index == 0,
                child: Text(VisibilityTypes.values[index].name),
              ),
            ),
            onChanged: (val) {
              selectedVisibilityType = val ?? VisibilityTypes.PUBLIC;
            },
          ),
          const SizedBox(
            height: 12,
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black54,
                  width: 1,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.circular(20)),
            padding: const EdgeInsets.all(12),
            child: Column(
              children: [
                const Text('Business Info'),
                const SizedBox(
                  height: 12,
                ),
                PhoneFormField(
                  controller: businessPhoneController,
                  value: account.phone,
                ),
                const SizedBox(
                  height: 12,
                ),
                EmailFormField(
                  controller: businessEmailController,
                  value: account.email,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }

  GestureDetector buildBirthdayField(BuildContext context, Account account) {
    return GestureDetector(
      onTap: () async {
        DateTime? selectedDate = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime.utc(1900),
          lastDate: DateTime.now(),
        );
        if (selectedDate != null) {
          setState(() {
            birthday = selectedDate;
          });
        }
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black54,
              width: 1,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.circular(20)),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            const Text('Birthday'),
            Text(
              UtilFunctions.intToFormattedDate(birthday.millisecondsSinceEpoch),
            ),
          ]),
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(Icons.arrow_back_ios)),
      actions: [
        IconButton(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                var newAccount = account.deepCopy();
                newAccount.username = usernameController.text;
                newAccount.displayName = nameController.text;
                newAccount.phone = phoneController.text;
                newAccount.email = emailController.text;
                newAccount.password = passwordController.text;
                newAccount.bio = bioController.text;
                newAccount.websiteUrl = websiteController.text;
                var newBusiness = BusinessInformation(
                  phone: businessPhoneController.text,
                  email: businessEmailController.text,
                  isActive: isBusinessAccountActive,
                );
                newAccount.business=newBusiness;
                newAccount.gender = selectedGenderType;
                newAccount.birthday =
                    $fixnum.Int64.fromInts(birthday.millisecondsSinceEpoch, 0);
                newAccount.visibility = selectedVisibilityType;
                try {
                  //TODO: replace when the service is back
                  // await updateProfileService(newAccount);
                  await mockUpdateProfileService(newAccount);
                  // ignore: use_build_context_synchronously
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Profile Updated!'),
                    ),
                  );
                  Get.back();
                } catch (_) {
                  UtilFunctions.showErrorSnackBar(
                      context, 'Something went wrong!');
                }
              }
            },
            icon: const Icon(Icons.check))
      ],
    );
  }
}
