import 'package:flutter/material.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_variables.dart';

class NameFormField extends StatelessWidget {
  const NameFormField({
    super.key,
    required this.controller,
    required this.value,
    required this.errorText,
    required this.hint, required this.label,
  });

  final TextEditingController controller;
  final String value;
  final String errorText;
  final String hint;
  final String label;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller..text = value,
      validator: (value) => UtilFunctions.nameValidator(
          value, errorText,),
      decoration:
          UtilVars.roundedInputDecoration.copyWith(hintText: hint,label: Text(label)),
    );
  }
}
