import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_functions.dart';
import 'package:iehsanamiri/presentation/widgets/utils/util_variables.dart';

class PhoneFormField extends StatelessWidget {
  const PhoneFormField({
    super.key,
    required this.controller,
    required this.value,
  });

  final TextEditingController controller;
  final String value;

  @override
  Widget build(BuildContext context) {
    return TextFormField(keyboardType: TextInputType.phone,
        controller: controller..text = value,
        validator: UtilFunctions.phoneNumberValidator,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        decoration: UtilVars.roundedInputDecoration
            .copyWith(hintText: "Enter your phone number",label: Text('Phone number')));
  }
}
