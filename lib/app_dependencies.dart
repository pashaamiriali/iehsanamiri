import 'package:get/get.dart';
import 'package:iehsanamiri/infrastructure/app_persistence.dart';

import 'infrastructure/client.dart';

class AppDependencies {
  static Future<void> init() async {
    Get.put(AppClient());
    var appPersistence=await AppPersistence.getInstance();
    Get.put( appPersistence);
  }
}
