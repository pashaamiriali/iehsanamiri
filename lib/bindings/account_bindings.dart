import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/account_controller.dart';
import 'package:iehsanamiri/controllers/login_controller.dart';

class AccountBindings extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
    Get.put(AccountController());
  }

}