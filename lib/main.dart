import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iehsanamiri/app_dependencies.dart';
import 'package:iehsanamiri/bindings/account_bindings.dart';
import 'package:iehsanamiri/mock_services/mock_profile_service.dart';
import 'package:iehsanamiri/presentation/home_screen/home_screen.dart';
import 'package:iehsanamiri/presentation/login_screen/login_screen.dart';
import 'package:iehsanamiri/services/profile_service.dart';

import 'infrastructure/app_persistence.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppDependencies.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'iehsanamiri',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: getHomeWidget(),
      initialBinding: AccountBindings(),
    );
  }

  Widget getHomeWidget() {
    if(Get.find<AppPersistence>().getLogin()){
      //todo: replace when server is up
      // getAccountService();
      mockGetProfileService();
      return const HomeScreen();
    }else {
      return const LoginScreen();
    }
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
