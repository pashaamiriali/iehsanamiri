import 'package:get/get.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';

class AccountController extends GetxController{
  final  account =Account().obs;
  final isLoading=true.obs;
  updateAccount(Account account){
    this.account(account);
  }
  clear(){
    account(Account());
  }

  void updateLoadingStatus(bool loading) {
    isLoading(loading);
  }
}