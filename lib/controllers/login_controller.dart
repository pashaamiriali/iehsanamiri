import 'package:get/get.dart';

class LoginController extends GetxController {
  final phoneNumber = ''.obs;
  final requestId = ''.obs;
  final password = ''.obs;

  updatePhoneNumber(String phoneNum) {
    phoneNumber(phoneNum);
  }

  updateRequestId(String reqId) {
    requestId(reqId);
  }

  updatePassword(String pass) {
    password(pass);
  }

  clear() {
    phoneNumber('');
    requestId('');
    password('');
  }
}
