import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/account_controller.dart';
import 'package:iehsanamiri/controllers/login_controller.dart';
import 'package:iehsanamiri/infrastructure/app_persistence.dart';

mockLogoutService() async {
  var persistence = Get.find<AppPersistence>();
  await persistence.logout();
  Get.find<LoginController>().clear();
  Get.find<AccountController>().clear();
}
