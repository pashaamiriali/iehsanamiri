import 'package:get/get.dart';
import 'package:iehsanamiri/infrastructure/app_persistence.dart';


/// returns the requestId
Future<String> mockSendOTPService(String phoneNumber) async {
  return 'testReq';
}

Future<void> mockVerifyOTPService(
    String verificationCode, String requestId) async {
  var persistence = Get.find<AppPersistence>();
  await persistence.login(
    'response.account.id',
    'response.token.accessToken',
    'response.token.refreshToken',
  );
}
