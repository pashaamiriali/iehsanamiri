import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/account_controller.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';

Future<Account> mockGetProfileService() async {
  final response = Account(
    id: '34524234',
    username: 'testuser',
    displayName: 'test user',
    phone: '01111023693',
    email: 'testUser@gmail.com',
    password: '132789asdf',
    gender: GenderTypes.MALE,
    bio: 'this is my bio',
    websiteUrl: 'example.com',
  );
  Get.find<AccountController>().updateAccount(response);
  Get.find<AccountController>().updateLoadingStatus(false);
  return response;
}
Future<Account> mockUpdateProfileService(Account account) async {
  Get.find<AccountController>().updateAccount(account);
  Get.find<AccountController>().updateLoadingStatus(false);
  return account;
}
