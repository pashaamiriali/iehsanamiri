import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/account_controller.dart';
import 'package:iehsanamiri/controllers/login_controller.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/infrastructure/app_persistence.dart';
import 'package:iehsanamiri/infrastructure/discovery.dart';

logoutService() async {
  var persistence = Get.find<AppPersistence>();
  //TODO: contact the backend for the problem with PERMISSION_DENIED exception
  await svc.account.logOut.rpcCall(Empty(),accessToken: persistence.getAccessToken());
  await persistence.logout();
  Get.find<LoginController>().clear();
  Get.find<AccountController>().clear();
}
