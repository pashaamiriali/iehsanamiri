import 'package:get/get.dart';
import 'package:grpc/grpc.dart';
import 'package:iehsanamiri/exceptions/app_exceptions.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/infrastructure/app_persistence.dart';

import 'package:iehsanamiri/infrastructure/discovery.dart';

/// returns the requestId
Future<String> sendOTPService(String phoneNumber) async {
  var altered = phoneNumber.substring(1, 11);
  try {
    var response = await svc.account.sendOtp.rpcCall(
        SendOtpRequest(email: '', phone: '98$altered'),
        accessToken: null);
    return response.requestId;
  } on Exception catch (e) {
    throw AppException(message: 'Connection failed!');
  }
}

Future<void> verifyOTPService(String verificationCode, String requestId) async {
  try {
    var response = await svc.account.verifyOtp.rpcCall(VerifyOtpRequest(
      password: verificationCode,
      requestId: requestId,
    ),accessToken: null);
    var persistence = Get.find<AppPersistence>();
    await persistence.login(
      response.account.id,
      response.token.accessToken,
      response.token.refreshToken,
    );
  } on GrpcError catch (e) {
    if (e.code == 14) {
      throw AppException(message: 'Connection failed!');
    } else if (e.code == 3) {
      throw AppException(message: 'Verification code is invalid!');
    } else {
      throw AppException(message: 'Something went wrong!');
    }
  }
}
