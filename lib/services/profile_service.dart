import 'package:get/get.dart';
import 'package:iehsanamiri/controllers/account_controller.dart';
import 'package:iehsanamiri/exceptions/app_exceptions.dart';
import 'package:iehsanamiri/infrastructure/api/services/account/account.pb.dart';
import 'package:iehsanamiri/infrastructure/app_persistence.dart';
import 'package:iehsanamiri/infrastructure/discovery.dart';

Future<Account> getProfileService() async {
  try {
    var persistence = Get.find<AppPersistence>();
    var userId = persistence.getUserId();
    String accessToken = persistence.getAccessToken();
    var response = await svc.account.getProfile.rpcCall(
      Account()..id = userId,
      accessToken: accessToken,
    );
    Get.find<AccountController>().updateAccount(response);
    Get.find<AccountController>().updateLoadingStatus(false);
    return response;
  } on Exception catch (_) {
    Get.find<AccountController>().updateLoadingStatus(false);
    throw AppException(message: 'Something went wrong');
  }
}

Future<Account> updateProfileService(Account account) async {
  try {
    Get.find<AccountController>().updateLoadingStatus(true);
    var persistence = Get.find<AppPersistence>();
    String accessToken = persistence.getAccessToken();
    var response = await svc.account.updateProfile.rpcCall(
      account,
      accessToken: accessToken,
    );
    Get.find<AccountController>().updateAccount(response);
    Get.find<AccountController>().updateLoadingStatus(false);
    return response;
  } on Exception catch (_) {
    Get.find<AccountController>().updateLoadingStatus(false);
    throw AppException(message: 'Something went wrong');  }
}
