import 'package:protobuf/protobuf.dart';
import 'package:grpc/grpc_connection_interface.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:grpc/grpc_or_grpcweb.dart';
import 'package:grpc/grpc.dart';
import 'dart:convert';
import 'dart:io';

import 'api/services/account/account.pbgrpc.dart';

class ServiceDiscovery {
  static const String _caCert = '''-----BEGIN CERTIFICATE-----
MIIEPTCCAyWgAwIBAgIUDCvy7vKR4CXl62NA7hV6gIIUhAwwDQYJKoZIhvcNAQEL
BQAwgY8xLDAqBgNVBAMMIyouc3ZjLnRvY3MuYXBwLCAqLnN2Yy50YWhsaWxkZXYu
Y29tMRowGAYJKoZIhvcNAQkBFgttYm5AZ214LmNvbTELMAkGA1UEBhMCVVMxCzAJ
BgNVBAgMAkRFMRMwEQYDVQQHDApXaWxtaW5ndG9uMRQwEgYDVQQKDAtUYWhsaWxn
YXJhbjAeFw0yMzAxMTUxNDAyMzFaFw00ODAxMDkxNDAyMzFaMIGPMSwwKgYDVQQD
DCMqLnN2Yy50b2NzLmFwcCwgKi5zdmMudGFobGlsZGV2LmNvbTEaMBgGCSqGSIb3
DQEJARYLbWJuQGdteC5jb20xCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJERTETMBEG
A1UEBwwKV2lsbWluZ3RvbjEUMBIGA1UECgwLVGFobGlsZ2FyYW4wggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDAcTvwriBLPPnQKE8hL9ShNA7zJzb3QVFB
Gy+E6uTtM6wfcUREEZhqZZi2JXCWJIs35WzSIWit945matlLeXvFFWV+MNRcMGyL
whI1VDEkKLXolf29RFJn8Bx0Lh9i1spyKRGO6yAspHGgLZ+sx8N0LWB/ccEtMFkt
kZ1AI2Kx5BxqHx15Ru4DoAkKfFrp3vlzCOk5J3AYVEXv2xnO5nJjcXeBUh0HzaBF
/jQ6comd3jVtfQcY/yGac7O4KvK8k0c48a5nOLtBwnsYqFhKabm1c3Fh+RAigXfK
FEtVvWZt3bZXRsw+AOucFA8essi0C4Evco9jDz+qeAJQVbtflTKhAgMBAAGjgY4w
gYswHQYDVR0OBBYEFI/F0Bs4dR5bEXG5x7RYXWU9DCpHMAwGA1UdEwEB/wQCMAAw
LgYDVR0RBCcwJYIOKi5zdmMudG9jcy5hcHCCEyouc3ZjLnRhaGxpbGRldi5jb20w
LAYJYIZIAYb4QgENBB8WHU9wZW5TU0wgR2VuZXJhdGVkIENlcnRpZmljYXRlMA0G
CSqGSIb3DQEBCwUAA4IBAQC7MGC/9zM2oAo6h/OFhyIs8nv1rvnGhCVsOKNG6czY
iG90q/1us0PrldmgJa7+J0NkYOx8N7/uTFp3W1CwtFbNvQ1c29YfWzY2DZbmkD37
u+t3aqqx051hJK11wUmdTT1WDVj7+yQn42rwDwMeG3jjePWiAx5V1839z25mNFMt
f7PGgkdLmXoJ0ym6qI8S9MsA4zyMEdo0DMzLCYPDXu8nl0KawEJ9+ARY/yJcfXJ9
qwvuH/skRTTzqHYPZhXcLA4ArCiEAUCBiGuV91C1mouy6lbNcpcmIEqX/SDH7yzq
2KUTjv57oRGCu1zxh3s2L/zHcv14fXJ6G/HDCY53Or97
-----END CERTIFICATE-----''';

  ClientChannelBase? _accountChannel;

  Client? _accountClient;

  List<int>? _certificate;

  List<int> get certificate {
    _certificate ??= utf8.encode(_caCert);
    return _certificate!;
  }

  ClientChannelBase buildClientChannel() {
    if (kIsWeb) {
      return GrpcOrGrpcWebClientChannel.toSingleEndpoint(
          host: 'account-web.svc.tahlildev.com',
          port: 443,
          transportSecure: true);
    }
    return ClientChannel(
      'account.svc.tahlildev.com',
      port: 443,
      options: ChannelOptions(
        credentials: ChannelCredentials.secure(
            certificates: certificate,
            onBadCertificate: (X509Certificate cert, String host) => true),
        idleTimeout: const Duration(seconds: 3),
        connectionTimeout: const Duration(seconds: 10),
      ),
    );
  }

  AccountServiceClient get account {
    if (_accountClient == null) {
      _accountChannel = buildClientChannel();
      _accountClient = AccountServiceClient(_accountChannel!);
    }
    return _accountClient! as AccountServiceClient;
  }

  void dispose() {
    _accountChannel!.shutdown();
  }
}

extension RpcCall<R extends GeneratedMessage, Q extends GeneratedMessage>
    on GrpcMethod<R, Q> {
  Future<R> rpcCall(Q? data, {String? accessToken}) {
    Map<String, String> metadata = {'network_id': '62c3cdc35fca15eef4eb5991'};
    if (accessToken != null) {
      metadata['access_token'] = accessToken;
    }
    return this(data ?? (Empty() as Q),
        options: CallOptions(
            timeout: const Duration(seconds: 10), metadata: metadata));
  }
}

typedef GrpcMethod<R extends GeneratedMessage, Q extends GeneratedMessage>
    = ResponseFuture<R> Function(Q data, {CallOptions? options});

final svc = ServiceDiscovery();
