///
//  Generated code. Do not modify.
//  source: services/account/account.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'account.pb.dart' as $0;
export 'account.pb.dart';

class AccountServiceClient extends $grpc.Client {
  static final _$sendOtp =
      $grpc.ClientMethod<$0.SendOtpRequest, $0.SendOtpResponse>(
          '/social.account.AccountService/SendOtp',
          ($0.SendOtpRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SendOtpResponse.fromBuffer(value));
  static final _$verifyOtp =
      $grpc.ClientMethod<$0.VerifyOtpRequest, $0.VerifyOtpResponse>(
          '/social.account.AccountService/VerifyOtp',
          ($0.VerifyOtpRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.VerifyOtpResponse.fromBuffer(value));
  static final _$logOut = $grpc.ClientMethod<$0.Empty, $0.Empty>(
      '/social.account.AccountService/LogOut',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Empty.fromBuffer(value));
  static final _$getProfile = $grpc.ClientMethod<$0.Account, $0.Account>(
      '/social.account.AccountService/GetProfile',
      ($0.Account value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Account.fromBuffer(value));
  static final _$updateProfile = $grpc.ClientMethod<$0.Account, $0.Account>(
      '/social.account.AccountService/UpdateProfile',
      ($0.Account value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Account.fromBuffer(value));

  AccountServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.SendOtpResponse> sendOtp($0.SendOtpRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendOtp, request, options: options);
  }

  $grpc.ResponseFuture<$0.VerifyOtpResponse> verifyOtp(
      $0.VerifyOtpRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$verifyOtp, request, options: options);
  }

  $grpc.ResponseFuture<$0.Empty> logOut($0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$logOut, request, options: options);
  }

  $grpc.ResponseFuture<$0.Account> getProfile($0.Account request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getProfile, request, options: options);
  }

  $grpc.ResponseFuture<$0.Account> updateProfile($0.Account request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateProfile, request, options: options);
  }
}

abstract class AccountServiceBase extends $grpc.Service {
  $core.String get $name => 'social.account.AccountService';

  AccountServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.SendOtpRequest, $0.SendOtpResponse>(
        'SendOtp',
        sendOtp_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SendOtpRequest.fromBuffer(value),
        ($0.SendOtpResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.VerifyOtpRequest, $0.VerifyOtpResponse>(
        'VerifyOtp',
        verifyOtp_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.VerifyOtpRequest.fromBuffer(value),
        ($0.VerifyOtpResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $0.Empty>(
        'LogOut',
        logOut_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Account, $0.Account>(
        'GetProfile',
        getProfile_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Account.fromBuffer(value),
        ($0.Account value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Account, $0.Account>(
        'UpdateProfile',
        updateProfile_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Account.fromBuffer(value),
        ($0.Account value) => value.writeToBuffer()));
  }

  $async.Future<$0.SendOtpResponse> sendOtp_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SendOtpRequest> request) async {
    return sendOtp(call, await request);
  }

  $async.Future<$0.VerifyOtpResponse> verifyOtp_Pre($grpc.ServiceCall call,
      $async.Future<$0.VerifyOtpRequest> request) async {
    return verifyOtp(call, await request);
  }

  $async.Future<$0.Empty> logOut_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return logOut(call, await request);
  }

  $async.Future<$0.Account> getProfile_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Account> request) async {
    return getProfile(call, await request);
  }

  $async.Future<$0.Account> updateProfile_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Account> request) async {
    return updateProfile(call, await request);
  }

  $async.Future<$0.SendOtpResponse> sendOtp(
      $grpc.ServiceCall call, $0.SendOtpRequest request);
  $async.Future<$0.VerifyOtpResponse> verifyOtp(
      $grpc.ServiceCall call, $0.VerifyOtpRequest request);
  $async.Future<$0.Empty> logOut($grpc.ServiceCall call, $0.Empty request);
  $async.Future<$0.Account> getProfile(
      $grpc.ServiceCall call, $0.Account request);
  $async.Future<$0.Account> updateProfile(
      $grpc.ServiceCall call, $0.Account request);
}
