///
//  Generated code. Do not modify.
//  source: services/account/account.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'account.pbenum.dart';

export 'account.pbenum.dart';

class SendOtpRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SendOtpRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..hasRequiredFields = false
  ;

  SendOtpRequest._() : super();
  factory SendOtpRequest({
    $core.String? phone,
    $core.String? email,
  }) {
    final _result = create();
    if (phone != null) {
      _result.phone = phone;
    }
    if (email != null) {
      _result.email = email;
    }
    return _result;
  }
  factory SendOtpRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SendOtpRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SendOtpRequest clone() => SendOtpRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SendOtpRequest copyWith(void Function(SendOtpRequest) updates) => super.copyWith((message) => updates(message as SendOtpRequest)) as SendOtpRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SendOtpRequest create() => SendOtpRequest._();
  SendOtpRequest createEmptyInstance() => create();
  static $pb.PbList<SendOtpRequest> createRepeated() => $pb.PbList<SendOtpRequest>();
  @$core.pragma('dart2js:noInline')
  static SendOtpRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SendOtpRequest>(create);
  static SendOtpRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get phone => $_getSZ(0);
  @$pb.TagNumber(1)
  set phone($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPhone() => $_has(0);
  @$pb.TagNumber(1)
  void clearPhone() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get email => $_getSZ(1);
  @$pb.TagNumber(2)
  set email($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmail() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmail() => clearField(2);
}

class SendOtpResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SendOtpResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'requestId')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'limit', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  SendOtpResponse._() : super();
  factory SendOtpResponse({
    $core.String? requestId,
    $core.int? limit,
  }) {
    final _result = create();
    if (requestId != null) {
      _result.requestId = requestId;
    }
    if (limit != null) {
      _result.limit = limit;
    }
    return _result;
  }
  factory SendOtpResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SendOtpResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SendOtpResponse clone() => SendOtpResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SendOtpResponse copyWith(void Function(SendOtpResponse) updates) => super.copyWith((message) => updates(message as SendOtpResponse)) as SendOtpResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SendOtpResponse create() => SendOtpResponse._();
  SendOtpResponse createEmptyInstance() => create();
  static $pb.PbList<SendOtpResponse> createRepeated() => $pb.PbList<SendOtpResponse>();
  @$core.pragma('dart2js:noInline')
  static SendOtpResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SendOtpResponse>(create);
  static SendOtpResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get requestId => $_getSZ(0);
  @$pb.TagNumber(1)
  set requestId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRequestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRequestId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get limit => $_getIZ(1);
  @$pb.TagNumber(2)
  set limit($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLimit() => $_has(1);
  @$pb.TagNumber(2)
  void clearLimit() => clearField(2);
}

class VerifyOtpRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VerifyOtpRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'requestId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'password')
    ..hasRequiredFields = false
  ;

  VerifyOtpRequest._() : super();
  factory VerifyOtpRequest({
    $core.String? requestId,
    $core.String? password,
  }) {
    final _result = create();
    if (requestId != null) {
      _result.requestId = requestId;
    }
    if (password != null) {
      _result.password = password;
    }
    return _result;
  }
  factory VerifyOtpRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerifyOtpRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VerifyOtpRequest clone() => VerifyOtpRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VerifyOtpRequest copyWith(void Function(VerifyOtpRequest) updates) => super.copyWith((message) => updates(message as VerifyOtpRequest)) as VerifyOtpRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerifyOtpRequest create() => VerifyOtpRequest._();
  VerifyOtpRequest createEmptyInstance() => create();
  static $pb.PbList<VerifyOtpRequest> createRepeated() => $pb.PbList<VerifyOtpRequest>();
  @$core.pragma('dart2js:noInline')
  static VerifyOtpRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerifyOtpRequest>(create);
  static VerifyOtpRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get requestId => $_getSZ(0);
  @$pb.TagNumber(1)
  set requestId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRequestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRequestId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class VerifyOtpResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VerifyOtpResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOM<Token>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token', subBuilder: Token.create)
    ..aOM<Account>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'account', subBuilder: Account.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'action')
    ..aOM<Network>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'network', subBuilder: Network.create)
    ..hasRequiredFields = false
  ;

  VerifyOtpResponse._() : super();
  factory VerifyOtpResponse({
    Token? token,
    Account? account,
    $core.String? action,
    Network? network,
  }) {
    final _result = create();
    if (token != null) {
      _result.token = token;
    }
    if (account != null) {
      _result.account = account;
    }
    if (action != null) {
      _result.action = action;
    }
    if (network != null) {
      _result.network = network;
    }
    return _result;
  }
  factory VerifyOtpResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerifyOtpResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VerifyOtpResponse clone() => VerifyOtpResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VerifyOtpResponse copyWith(void Function(VerifyOtpResponse) updates) => super.copyWith((message) => updates(message as VerifyOtpResponse)) as VerifyOtpResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerifyOtpResponse create() => VerifyOtpResponse._();
  VerifyOtpResponse createEmptyInstance() => create();
  static $pb.PbList<VerifyOtpResponse> createRepeated() => $pb.PbList<VerifyOtpResponse>();
  @$core.pragma('dart2js:noInline')
  static VerifyOtpResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerifyOtpResponse>(create);
  static VerifyOtpResponse? _defaultInstance;

  @$pb.TagNumber(1)
  Token get token => $_getN(0);
  @$pb.TagNumber(1)
  set token(Token v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);
  @$pb.TagNumber(1)
  Token ensureToken() => $_ensure(0);

  @$pb.TagNumber(2)
  Account get account => $_getN(1);
  @$pb.TagNumber(2)
  set account(Account v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAccount() => $_has(1);
  @$pb.TagNumber(2)
  void clearAccount() => clearField(2);
  @$pb.TagNumber(2)
  Account ensureAccount() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get action => $_getSZ(2);
  @$pb.TagNumber(3)
  set action($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAction() => $_has(2);
  @$pb.TagNumber(3)
  void clearAction() => clearField(3);

  @$pb.TagNumber(4)
  Network get network => $_getN(3);
  @$pb.TagNumber(4)
  set network(Network v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasNetwork() => $_has(3);
  @$pb.TagNumber(4)
  void clearNetwork() => clearField(4);
  @$pb.TagNumber(4)
  Network ensureNetwork() => $_ensure(3);
}

class Account extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Account', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'networkId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'username')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'displayName')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'password')
    ..e<GenderTypes>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gender', $pb.PbFieldType.OE, defaultOrMaker: GenderTypes.None, valueOf: GenderTypes.valueOf, enumValues: GenderTypes.values)
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bio')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'websiteUrl')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatarUrl')
    ..aInt64(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'birthday')
    ..aInt64(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt')
    ..aInt64(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'disabledAt')
    ..aInt64(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deletedAt')
    ..aInt64(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bannedAt')
    ..e<VisibilityTypes>(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'visibility', $pb.PbFieldType.OE, defaultOrMaker: VisibilityTypes.PUBLIC, valueOf: VisibilityTypes.valueOf, enumValues: VisibilityTypes.values)
    ..aOB(18, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isBlocker')
    ..aOB(19, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isBlocking')
    ..aOM<AccountStat>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stat', subBuilder: AccountStat.create)
    ..aOB(21, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isVerified')
    ..aOM<BusinessInformation>(22, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'business', subBuilder: BusinessInformation.create)
    ..aOM<Notification>(23, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'notification', subBuilder: Notification.create)
    ..aOB(24, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hasAvatar')
    ..aOS(25, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'role')
    ..aOS(26, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'referralCode')
    ..a<$core.int>(27, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedAt', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Account._() : super();
  factory Account({
    $core.String? id,
    $core.String? networkId,
    $core.String? username,
    $core.String? displayName,
    $core.String? phone,
    $core.String? email,
    $core.String? password,
    GenderTypes? gender,
    $core.String? bio,
    $core.String? websiteUrl,
    $core.String? avatarUrl,
    $fixnum.Int64? birthday,
    $fixnum.Int64? createdAt,
    $fixnum.Int64? disabledAt,
    $fixnum.Int64? deletedAt,
    $fixnum.Int64? bannedAt,
    VisibilityTypes? visibility,
    $core.bool? isBlocker,
    $core.bool? isBlocking,
    AccountStat? stat,
    $core.bool? isVerified,
    BusinessInformation? business,
    Notification? notification,
    $core.bool? hasAvatar,
    $core.String? role,
    $core.String? referralCode,
    $core.int? updatedAt,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (networkId != null) {
      _result.networkId = networkId;
    }
    if (username != null) {
      _result.username = username;
    }
    if (displayName != null) {
      _result.displayName = displayName;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    if (email != null) {
      _result.email = email;
    }
    if (password != null) {
      _result.password = password;
    }
    if (gender != null) {
      _result.gender = gender;
    }
    if (bio != null) {
      _result.bio = bio;
    }
    if (websiteUrl != null) {
      _result.websiteUrl = websiteUrl;
    }
    if (avatarUrl != null) {
      _result.avatarUrl = avatarUrl;
    }
    if (birthday != null) {
      _result.birthday = birthday;
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (disabledAt != null) {
      _result.disabledAt = disabledAt;
    }
    if (deletedAt != null) {
      _result.deletedAt = deletedAt;
    }
    if (bannedAt != null) {
      _result.bannedAt = bannedAt;
    }
    if (visibility != null) {
      _result.visibility = visibility;
    }
    if (isBlocker != null) {
      _result.isBlocker = isBlocker;
    }
    if (isBlocking != null) {
      _result.isBlocking = isBlocking;
    }
    if (stat != null) {
      _result.stat = stat;
    }
    if (isVerified != null) {
      _result.isVerified = isVerified;
    }
    if (business != null) {
      _result.business = business;
    }
    if (notification != null) {
      _result.notification = notification;
    }
    if (hasAvatar != null) {
      _result.hasAvatar = hasAvatar;
    }
    if (role != null) {
      _result.role = role;
    }
    if (referralCode != null) {
      _result.referralCode = referralCode;
    }
    if (updatedAt != null) {
      _result.updatedAt = updatedAt;
    }
    return _result;
  }
  factory Account.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Account.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Account clone() => Account()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Account copyWith(void Function(Account) updates) => super.copyWith((message) => updates(message as Account)) as Account; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Account create() => Account._();
  Account createEmptyInstance() => create();
  static $pb.PbList<Account> createRepeated() => $pb.PbList<Account>();
  @$core.pragma('dart2js:noInline')
  static Account getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Account>(create);
  static Account? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get networkId => $_getSZ(1);
  @$pb.TagNumber(2)
  set networkId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNetworkId() => $_has(1);
  @$pb.TagNumber(2)
  void clearNetworkId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get username => $_getSZ(2);
  @$pb.TagNumber(3)
  set username($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasUsername() => $_has(2);
  @$pb.TagNumber(3)
  void clearUsername() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get displayName => $_getSZ(3);
  @$pb.TagNumber(4)
  set displayName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDisplayName() => $_has(3);
  @$pb.TagNumber(4)
  void clearDisplayName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get phone => $_getSZ(4);
  @$pb.TagNumber(5)
  set phone($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPhone() => $_has(4);
  @$pb.TagNumber(5)
  void clearPhone() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get email => $_getSZ(5);
  @$pb.TagNumber(6)
  set email($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasEmail() => $_has(5);
  @$pb.TagNumber(6)
  void clearEmail() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get password => $_getSZ(6);
  @$pb.TagNumber(7)
  set password($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasPassword() => $_has(6);
  @$pb.TagNumber(7)
  void clearPassword() => clearField(7);

  @$pb.TagNumber(8)
  GenderTypes get gender => $_getN(7);
  @$pb.TagNumber(8)
  set gender(GenderTypes v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasGender() => $_has(7);
  @$pb.TagNumber(8)
  void clearGender() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get bio => $_getSZ(8);
  @$pb.TagNumber(9)
  set bio($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasBio() => $_has(8);
  @$pb.TagNumber(9)
  void clearBio() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get websiteUrl => $_getSZ(9);
  @$pb.TagNumber(10)
  set websiteUrl($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasWebsiteUrl() => $_has(9);
  @$pb.TagNumber(10)
  void clearWebsiteUrl() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get avatarUrl => $_getSZ(10);
  @$pb.TagNumber(11)
  set avatarUrl($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasAvatarUrl() => $_has(10);
  @$pb.TagNumber(11)
  void clearAvatarUrl() => clearField(11);

  @$pb.TagNumber(12)
  $fixnum.Int64 get birthday => $_getI64(11);
  @$pb.TagNumber(12)
  set birthday($fixnum.Int64 v) { $_setInt64(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasBirthday() => $_has(11);
  @$pb.TagNumber(12)
  void clearBirthday() => clearField(12);

  @$pb.TagNumber(13)
  $fixnum.Int64 get createdAt => $_getI64(12);
  @$pb.TagNumber(13)
  set createdAt($fixnum.Int64 v) { $_setInt64(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasCreatedAt() => $_has(12);
  @$pb.TagNumber(13)
  void clearCreatedAt() => clearField(13);

  @$pb.TagNumber(14)
  $fixnum.Int64 get disabledAt => $_getI64(13);
  @$pb.TagNumber(14)
  set disabledAt($fixnum.Int64 v) { $_setInt64(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasDisabledAt() => $_has(13);
  @$pb.TagNumber(14)
  void clearDisabledAt() => clearField(14);

  @$pb.TagNumber(15)
  $fixnum.Int64 get deletedAt => $_getI64(14);
  @$pb.TagNumber(15)
  set deletedAt($fixnum.Int64 v) { $_setInt64(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasDeletedAt() => $_has(14);
  @$pb.TagNumber(15)
  void clearDeletedAt() => clearField(15);

  @$pb.TagNumber(16)
  $fixnum.Int64 get bannedAt => $_getI64(15);
  @$pb.TagNumber(16)
  set bannedAt($fixnum.Int64 v) { $_setInt64(15, v); }
  @$pb.TagNumber(16)
  $core.bool hasBannedAt() => $_has(15);
  @$pb.TagNumber(16)
  void clearBannedAt() => clearField(16);

  @$pb.TagNumber(17)
  VisibilityTypes get visibility => $_getN(16);
  @$pb.TagNumber(17)
  set visibility(VisibilityTypes v) { setField(17, v); }
  @$pb.TagNumber(17)
  $core.bool hasVisibility() => $_has(16);
  @$pb.TagNumber(17)
  void clearVisibility() => clearField(17);

  @$pb.TagNumber(18)
  $core.bool get isBlocker => $_getBF(17);
  @$pb.TagNumber(18)
  set isBlocker($core.bool v) { $_setBool(17, v); }
  @$pb.TagNumber(18)
  $core.bool hasIsBlocker() => $_has(17);
  @$pb.TagNumber(18)
  void clearIsBlocker() => clearField(18);

  @$pb.TagNumber(19)
  $core.bool get isBlocking => $_getBF(18);
  @$pb.TagNumber(19)
  set isBlocking($core.bool v) { $_setBool(18, v); }
  @$pb.TagNumber(19)
  $core.bool hasIsBlocking() => $_has(18);
  @$pb.TagNumber(19)
  void clearIsBlocking() => clearField(19);

  @$pb.TagNumber(20)
  AccountStat get stat => $_getN(19);
  @$pb.TagNumber(20)
  set stat(AccountStat v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasStat() => $_has(19);
  @$pb.TagNumber(20)
  void clearStat() => clearField(20);
  @$pb.TagNumber(20)
  AccountStat ensureStat() => $_ensure(19);

  @$pb.TagNumber(21)
  $core.bool get isVerified => $_getBF(20);
  @$pb.TagNumber(21)
  set isVerified($core.bool v) { $_setBool(20, v); }
  @$pb.TagNumber(21)
  $core.bool hasIsVerified() => $_has(20);
  @$pb.TagNumber(21)
  void clearIsVerified() => clearField(21);

  @$pb.TagNumber(22)
  BusinessInformation get business => $_getN(21);
  @$pb.TagNumber(22)
  set business(BusinessInformation v) { setField(22, v); }
  @$pb.TagNumber(22)
  $core.bool hasBusiness() => $_has(21);
  @$pb.TagNumber(22)
  void clearBusiness() => clearField(22);
  @$pb.TagNumber(22)
  BusinessInformation ensureBusiness() => $_ensure(21);

  @$pb.TagNumber(23)
  Notification get notification => $_getN(22);
  @$pb.TagNumber(23)
  set notification(Notification v) { setField(23, v); }
  @$pb.TagNumber(23)
  $core.bool hasNotification() => $_has(22);
  @$pb.TagNumber(23)
  void clearNotification() => clearField(23);
  @$pb.TagNumber(23)
  Notification ensureNotification() => $_ensure(22);

  @$pb.TagNumber(24)
  $core.bool get hasAvatar => $_getBF(23);
  @$pb.TagNumber(24)
  set hasAvatar($core.bool v) { $_setBool(23, v); }
  @$pb.TagNumber(24)
  $core.bool hasHasAvatar() => $_has(23);
  @$pb.TagNumber(24)
  void clearHasAvatar() => clearField(24);

  @$pb.TagNumber(25)
  $core.String get role => $_getSZ(24);
  @$pb.TagNumber(25)
  set role($core.String v) { $_setString(24, v); }
  @$pb.TagNumber(25)
  $core.bool hasRole() => $_has(24);
  @$pb.TagNumber(25)
  void clearRole() => clearField(25);

  @$pb.TagNumber(26)
  $core.String get referralCode => $_getSZ(25);
  @$pb.TagNumber(26)
  set referralCode($core.String v) { $_setString(25, v); }
  @$pb.TagNumber(26)
  $core.bool hasReferralCode() => $_has(25);
  @$pb.TagNumber(26)
  void clearReferralCode() => clearField(26);

  @$pb.TagNumber(27)
  $core.int get updatedAt => $_getIZ(26);
  @$pb.TagNumber(27)
  set updatedAt($core.int v) { $_setSignedInt32(26, v); }
  @$pb.TagNumber(27)
  $core.bool hasUpdatedAt() => $_has(26);
  @$pb.TagNumber(27)
  void clearUpdatedAt() => clearField(27);
}

class BusinessInformation extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BusinessInformation', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'category')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isActive')
    ..hasRequiredFields = false
  ;

  BusinessInformation._() : super();
  factory BusinessInformation({
    $core.String? phone,
    $core.String? email,
    $core.String? category,
    $core.bool? isActive,
  }) {
    final _result = create();
    if (phone != null) {
      _result.phone = phone;
    }
    if (email != null) {
      _result.email = email;
    }
    if (category != null) {
      _result.category = category;
    }
    if (isActive != null) {
      _result.isActive = isActive;
    }
    return _result;
  }
  factory BusinessInformation.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BusinessInformation.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BusinessInformation clone() => BusinessInformation()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BusinessInformation copyWith(void Function(BusinessInformation) updates) => super.copyWith((message) => updates(message as BusinessInformation)) as BusinessInformation; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BusinessInformation create() => BusinessInformation._();
  BusinessInformation createEmptyInstance() => create();
  static $pb.PbList<BusinessInformation> createRepeated() => $pb.PbList<BusinessInformation>();
  @$core.pragma('dart2js:noInline')
  static BusinessInformation getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BusinessInformation>(create);
  static BusinessInformation? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get phone => $_getSZ(0);
  @$pb.TagNumber(1)
  set phone($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPhone() => $_has(0);
  @$pb.TagNumber(1)
  void clearPhone() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get email => $_getSZ(1);
  @$pb.TagNumber(2)
  set email($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmail() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmail() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get category => $_getSZ(2);
  @$pb.TagNumber(3)
  set category($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCategory() => $_has(2);
  @$pb.TagNumber(3)
  void clearCategory() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get isActive => $_getBF(3);
  @$pb.TagNumber(4)
  set isActive($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIsActive() => $_has(3);
  @$pb.TagNumber(4)
  void clearIsActive() => clearField(4);
}

class AccountStat extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccountStat', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'postCount')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'followerCount')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'followingCount')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isFollower', $pb.PbFieldType.O3)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isFollowing', $pb.PbFieldType.O3)
    ..aOB(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hasStory')
    ..aOB(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isSeenAllStories')
    ..hasRequiredFields = false
  ;

  AccountStat._() : super();
  factory AccountStat({
    $fixnum.Int64? postCount,
    $fixnum.Int64? followerCount,
    $fixnum.Int64? followingCount,
    $core.int? isFollower,
    $core.int? isFollowing,
    $core.bool? hasStory,
    $core.bool? isSeenAllStories,
  }) {
    final _result = create();
    if (postCount != null) {
      _result.postCount = postCount;
    }
    if (followerCount != null) {
      _result.followerCount = followerCount;
    }
    if (followingCount != null) {
      _result.followingCount = followingCount;
    }
    if (isFollower != null) {
      _result.isFollower = isFollower;
    }
    if (isFollowing != null) {
      _result.isFollowing = isFollowing;
    }
    if (hasStory != null) {
      _result.hasStory = hasStory;
    }
    if (isSeenAllStories != null) {
      _result.isSeenAllStories = isSeenAllStories;
    }
    return _result;
  }
  factory AccountStat.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AccountStat.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AccountStat clone() => AccountStat()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AccountStat copyWith(void Function(AccountStat) updates) => super.copyWith((message) => updates(message as AccountStat)) as AccountStat; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AccountStat create() => AccountStat._();
  AccountStat createEmptyInstance() => create();
  static $pb.PbList<AccountStat> createRepeated() => $pb.PbList<AccountStat>();
  @$core.pragma('dart2js:noInline')
  static AccountStat getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AccountStat>(create);
  static AccountStat? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get postCount => $_getI64(0);
  @$pb.TagNumber(1)
  set postCount($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPostCount() => $_has(0);
  @$pb.TagNumber(1)
  void clearPostCount() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get followerCount => $_getI64(1);
  @$pb.TagNumber(2)
  set followerCount($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFollowerCount() => $_has(1);
  @$pb.TagNumber(2)
  void clearFollowerCount() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get followingCount => $_getI64(2);
  @$pb.TagNumber(3)
  set followingCount($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasFollowingCount() => $_has(2);
  @$pb.TagNumber(3)
  void clearFollowingCount() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get isFollower => $_getIZ(3);
  @$pb.TagNumber(4)
  set isFollower($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIsFollower() => $_has(3);
  @$pb.TagNumber(4)
  void clearIsFollower() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get isFollowing => $_getIZ(4);
  @$pb.TagNumber(5)
  set isFollowing($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasIsFollowing() => $_has(4);
  @$pb.TagNumber(5)
  void clearIsFollowing() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get hasStory => $_getBF(5);
  @$pb.TagNumber(6)
  set hasStory($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasHasStory() => $_has(5);
  @$pb.TagNumber(6)
  void clearHasStory() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get isSeenAllStories => $_getBF(6);
  @$pb.TagNumber(7)
  set isSeenAllStories($core.bool v) { $_setBool(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasIsSeenAllStories() => $_has(6);
  @$pb.TagNumber(7)
  void clearIsSeenAllStories() => clearField(7);
}

class Notification extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Notification', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'on')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'like', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'follow', $pb.PbFieldType.O3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'acceptFollow', $pb.PbFieldType.O3)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'comment', $pb.PbFieldType.O3)
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'commentLike', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Notification._() : super();
  factory Notification({
    $core.bool? on,
    $core.int? like,
    $core.int? follow,
    $core.int? acceptFollow,
    $core.int? comment,
    $core.int? commentLike,
  }) {
    final _result = create();
    if (on != null) {
      _result.on = on;
    }
    if (like != null) {
      _result.like = like;
    }
    if (follow != null) {
      _result.follow = follow;
    }
    if (acceptFollow != null) {
      _result.acceptFollow = acceptFollow;
    }
    if (comment != null) {
      _result.comment = comment;
    }
    if (commentLike != null) {
      _result.commentLike = commentLike;
    }
    return _result;
  }
  factory Notification.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Notification.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Notification clone() => Notification()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Notification copyWith(void Function(Notification) updates) => super.copyWith((message) => updates(message as Notification)) as Notification; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Notification create() => Notification._();
  Notification createEmptyInstance() => create();
  static $pb.PbList<Notification> createRepeated() => $pb.PbList<Notification>();
  @$core.pragma('dart2js:noInline')
  static Notification getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Notification>(create);
  static Notification? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get on => $_getBF(0);
  @$pb.TagNumber(1)
  set on($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOn() => $_has(0);
  @$pb.TagNumber(1)
  void clearOn() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get like => $_getIZ(1);
  @$pb.TagNumber(2)
  set like($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLike() => $_has(1);
  @$pb.TagNumber(2)
  void clearLike() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get follow => $_getIZ(2);
  @$pb.TagNumber(3)
  set follow($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasFollow() => $_has(2);
  @$pb.TagNumber(3)
  void clearFollow() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get acceptFollow => $_getIZ(3);
  @$pb.TagNumber(4)
  set acceptFollow($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasAcceptFollow() => $_has(3);
  @$pb.TagNumber(4)
  void clearAcceptFollow() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get comment => $_getIZ(4);
  @$pb.TagNumber(5)
  set comment($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasComment() => $_has(4);
  @$pb.TagNumber(5)
  void clearComment() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get commentLike => $_getIZ(5);
  @$pb.TagNumber(6)
  set commentLike($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasCommentLike() => $_has(5);
  @$pb.TagNumber(6)
  void clearCommentLike() => clearField(6);
}

class Token extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Token', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'accessToken')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'refreshToken')
    ..hasRequiredFields = false
  ;

  Token._() : super();
  factory Token({
    $core.String? accessToken,
    $core.String? refreshToken,
  }) {
    final _result = create();
    if (accessToken != null) {
      _result.accessToken = accessToken;
    }
    if (refreshToken != null) {
      _result.refreshToken = refreshToken;
    }
    return _result;
  }
  factory Token.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Token.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Token clone() => Token()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Token copyWith(void Function(Token) updates) => super.copyWith((message) => updates(message as Token)) as Token; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Token create() => Token._();
  Token createEmptyInstance() => create();
  static $pb.PbList<Token> createRepeated() => $pb.PbList<Token>();
  @$core.pragma('dart2js:noInline')
  static Token getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Token>(create);
  static Token? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get accessToken => $_getSZ(0);
  @$pb.TagNumber(1)
  set accessToken($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAccessToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearAccessToken() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get refreshToken => $_getSZ(1);
  @$pb.TagNumber(2)
  set refreshToken($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRefreshToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearRefreshToken() => clearField(2);
}

class Network_Settings_NetworkLoginProvider extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Network.Settings.NetworkLoginProvider', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..e<LoginProvidersEnum>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'provider', $pb.PbFieldType.OE, defaultOrMaker: LoginProvidersEnum.SMS, valueOf: LoginProvidersEnum.valueOf, enumValues: LoginProvidersEnum.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'config')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isActive')
    ..hasRequiredFields = false
  ;

  Network_Settings_NetworkLoginProvider._() : super();
  factory Network_Settings_NetworkLoginProvider({
    LoginProvidersEnum? provider,
    $core.String? config,
    $core.bool? isActive,
  }) {
    final _result = create();
    if (provider != null) {
      _result.provider = provider;
    }
    if (config != null) {
      _result.config = config;
    }
    if (isActive != null) {
      _result.isActive = isActive;
    }
    return _result;
  }
  factory Network_Settings_NetworkLoginProvider.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Network_Settings_NetworkLoginProvider.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Network_Settings_NetworkLoginProvider clone() => Network_Settings_NetworkLoginProvider()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Network_Settings_NetworkLoginProvider copyWith(void Function(Network_Settings_NetworkLoginProvider) updates) => super.copyWith((message) => updates(message as Network_Settings_NetworkLoginProvider)) as Network_Settings_NetworkLoginProvider; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Network_Settings_NetworkLoginProvider create() => Network_Settings_NetworkLoginProvider._();
  Network_Settings_NetworkLoginProvider createEmptyInstance() => create();
  static $pb.PbList<Network_Settings_NetworkLoginProvider> createRepeated() => $pb.PbList<Network_Settings_NetworkLoginProvider>();
  @$core.pragma('dart2js:noInline')
  static Network_Settings_NetworkLoginProvider getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Network_Settings_NetworkLoginProvider>(create);
  static Network_Settings_NetworkLoginProvider? _defaultInstance;

  @$pb.TagNumber(1)
  LoginProvidersEnum get provider => $_getN(0);
  @$pb.TagNumber(1)
  set provider(LoginProvidersEnum v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasProvider() => $_has(0);
  @$pb.TagNumber(1)
  void clearProvider() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get config => $_getSZ(1);
  @$pb.TagNumber(2)
  set config($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasConfig() => $_has(1);
  @$pb.TagNumber(2)
  void clearConfig() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get isActive => $_getBF(2);
  @$pb.TagNumber(3)
  set isActive($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasIsActive() => $_has(2);
  @$pb.TagNumber(3)
  void clearIsActive() => clearField(3);
}

class Network_Settings extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Network.Settings', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isActive')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isSignupActive')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isPublicAccessActive')
    ..pc<Network_Settings_NetworkLoginProvider>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'loginProviders', $pb.PbFieldType.PM, subBuilder: Network_Settings_NetworkLoginProvider.create)
    ..hasRequiredFields = false
  ;

  Network_Settings._() : super();
  factory Network_Settings({
    $core.bool? isActive,
    $core.bool? isSignupActive,
    $core.bool? isPublicAccessActive,
    $core.Iterable<Network_Settings_NetworkLoginProvider>? loginProviders,
  }) {
    final _result = create();
    if (isActive != null) {
      _result.isActive = isActive;
    }
    if (isSignupActive != null) {
      _result.isSignupActive = isSignupActive;
    }
    if (isPublicAccessActive != null) {
      _result.isPublicAccessActive = isPublicAccessActive;
    }
    if (loginProviders != null) {
      _result.loginProviders.addAll(loginProviders);
    }
    return _result;
  }
  factory Network_Settings.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Network_Settings.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Network_Settings clone() => Network_Settings()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Network_Settings copyWith(void Function(Network_Settings) updates) => super.copyWith((message) => updates(message as Network_Settings)) as Network_Settings; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Network_Settings create() => Network_Settings._();
  Network_Settings createEmptyInstance() => create();
  static $pb.PbList<Network_Settings> createRepeated() => $pb.PbList<Network_Settings>();
  @$core.pragma('dart2js:noInline')
  static Network_Settings getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Network_Settings>(create);
  static Network_Settings? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get isActive => $_getBF(0);
  @$pb.TagNumber(1)
  set isActive($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIsActive() => $_has(0);
  @$pb.TagNumber(1)
  void clearIsActive() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get isSignupActive => $_getBF(1);
  @$pb.TagNumber(2)
  set isSignupActive($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasIsSignupActive() => $_has(1);
  @$pb.TagNumber(2)
  void clearIsSignupActive() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get isPublicAccessActive => $_getBF(2);
  @$pb.TagNumber(3)
  set isPublicAccessActive($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasIsPublicAccessActive() => $_has(2);
  @$pb.TagNumber(3)
  void clearIsPublicAccessActive() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<Network_Settings_NetworkLoginProvider> get loginProviders => $_getList(3);
}

class Network extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Network', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'username')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'websiteUrl')
    ..aOM<Network_Settings>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'settings', subBuilder: Network_Settings.create)
    ..aOB(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hasAvatar')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatarUrl')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'titleFa')
    ..hasRequiredFields = false
  ;

  Network._() : super();
  factory Network({
    $core.String? id,
    $core.String? title,
    $core.String? username,
    $core.String? description,
    $core.String? websiteUrl,
    Network_Settings? settings,
    $core.bool? hasAvatar,
    $core.String? avatarUrl,
    $core.String? titleFa,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (title != null) {
      _result.title = title;
    }
    if (username != null) {
      _result.username = username;
    }
    if (description != null) {
      _result.description = description;
    }
    if (websiteUrl != null) {
      _result.websiteUrl = websiteUrl;
    }
    if (settings != null) {
      _result.settings = settings;
    }
    if (hasAvatar != null) {
      _result.hasAvatar = hasAvatar;
    }
    if (avatarUrl != null) {
      _result.avatarUrl = avatarUrl;
    }
    if (titleFa != null) {
      _result.titleFa = titleFa;
    }
    return _result;
  }
  factory Network.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Network.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Network clone() => Network()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Network copyWith(void Function(Network) updates) => super.copyWith((message) => updates(message as Network)) as Network; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Network create() => Network._();
  Network createEmptyInstance() => create();
  static $pb.PbList<Network> createRepeated() => $pb.PbList<Network>();
  @$core.pragma('dart2js:noInline')
  static Network getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Network>(create);
  static Network? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get username => $_getSZ(2);
  @$pb.TagNumber(3)
  set username($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasUsername() => $_has(2);
  @$pb.TagNumber(3)
  void clearUsername() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get description => $_getSZ(3);
  @$pb.TagNumber(4)
  set description($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDescription() => $_has(3);
  @$pb.TagNumber(4)
  void clearDescription() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get websiteUrl => $_getSZ(4);
  @$pb.TagNumber(5)
  set websiteUrl($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasWebsiteUrl() => $_has(4);
  @$pb.TagNumber(5)
  void clearWebsiteUrl() => clearField(5);

  @$pb.TagNumber(6)
  Network_Settings get settings => $_getN(5);
  @$pb.TagNumber(6)
  set settings(Network_Settings v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasSettings() => $_has(5);
  @$pb.TagNumber(6)
  void clearSettings() => clearField(6);
  @$pb.TagNumber(6)
  Network_Settings ensureSettings() => $_ensure(5);

  @$pb.TagNumber(7)
  $core.bool get hasAvatar => $_getBF(6);
  @$pb.TagNumber(7)
  set hasAvatar($core.bool v) { $_setBool(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasHasAvatar() => $_has(6);
  @$pb.TagNumber(7)
  void clearHasAvatar() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get avatarUrl => $_getSZ(7);
  @$pb.TagNumber(8)
  set avatarUrl($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasAvatarUrl() => $_has(7);
  @$pb.TagNumber(8)
  void clearAvatarUrl() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get titleFa => $_getSZ(8);
  @$pb.TagNumber(9)
  set titleFa($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasTitleFa() => $_has(8);
  @$pb.TagNumber(9)
  void clearTitleFa() => clearField(9);
}

class Empty extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Empty', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'social.account'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Empty._() : super();
  factory Empty() => create();
  factory Empty.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Empty.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Empty clone() => Empty()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Empty copyWith(void Function(Empty) updates) => super.copyWith((message) => updates(message as Empty)) as Empty; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Empty create() => Empty._();
  Empty createEmptyInstance() => create();
  static $pb.PbList<Empty> createRepeated() => $pb.PbList<Empty>();
  @$core.pragma('dart2js:noInline')
  static Empty getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Empty>(create);
  static Empty? _defaultInstance;
}

