///
//  Generated code. Do not modify.
//  source: services/account/account.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class GenderTypes extends $pb.ProtobufEnum {
  static const GenderTypes None = GenderTypes._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'None');
  static const GenderTypes MALE = GenderTypes._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MALE');
  static const GenderTypes FEMALE = GenderTypes._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'FEMALE');

  static const $core.List<GenderTypes> values = <GenderTypes> [
    None,
    MALE,
    FEMALE,
  ];

  static final $core.Map<$core.int, GenderTypes> _byValue = $pb.ProtobufEnum.initByValue(values);
  static GenderTypes? valueOf($core.int value) => _byValue[value];

  const GenderTypes._($core.int v, $core.String n) : super(v, n);
}

class VisibilityTypes extends $pb.ProtobufEnum {
  static const VisibilityTypes PUBLIC = VisibilityTypes._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PUBLIC');
  static const VisibilityTypes PRIVATE = VisibilityTypes._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PRIVATE');
  static const VisibilityTypes NETWORK = VisibilityTypes._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NETWORK');

  static const $core.List<VisibilityTypes> values = <VisibilityTypes> [
    PUBLIC,
    PRIVATE,
    NETWORK,
  ];

  static final $core.Map<$core.int, VisibilityTypes> _byValue = $pb.ProtobufEnum.initByValue(values);
  static VisibilityTypes? valueOf($core.int value) => _byValue[value];

  const VisibilityTypes._($core.int v, $core.String n) : super(v, n);
}

class LoginProvidersEnum extends $pb.ProtobufEnum {
  static const LoginProvidersEnum SMS = LoginProvidersEnum._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SMS');
  static const LoginProvidersEnum EMAIL = LoginProvidersEnum._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'EMAIL');
  static const LoginProvidersEnum FACEBOOK = LoginProvidersEnum._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'FACEBOOK');
  static const LoginProvidersEnum GOOGLE = LoginProvidersEnum._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'GOOGLE');

  static const $core.List<LoginProvidersEnum> values = <LoginProvidersEnum> [
    SMS,
    EMAIL,
    FACEBOOK,
    GOOGLE,
  ];

  static final $core.Map<$core.int, LoginProvidersEnum> _byValue = $pb.ProtobufEnum.initByValue(values);
  static LoginProvidersEnum? valueOf($core.int value) => _byValue[value];

  const LoginProvidersEnum._($core.int v, $core.String n) : super(v, n);
}

