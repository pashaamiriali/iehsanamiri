///
//  Generated code. Do not modify.
//  source: services/account/account.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use genderTypesDescriptor instead')
const GenderTypes$json = const {
  '1': 'GenderTypes',
  '2': const [
    const {'1': 'None', '2': 0},
    const {'1': 'MALE', '2': 1},
    const {'1': 'FEMALE', '2': 2},
  ],
};

/// Descriptor for `GenderTypes`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List genderTypesDescriptor = $convert.base64Decode('CgtHZW5kZXJUeXBlcxIICgROb25lEAASCAoETUFMRRABEgoKBkZFTUFMRRAC');
@$core.Deprecated('Use visibilityTypesDescriptor instead')
const VisibilityTypes$json = const {
  '1': 'VisibilityTypes',
  '2': const [
    const {'1': 'PUBLIC', '2': 0},
    const {'1': 'PRIVATE', '2': 1},
    const {'1': 'NETWORK', '2': 2},
  ],
};

/// Descriptor for `VisibilityTypes`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List visibilityTypesDescriptor = $convert.base64Decode('Cg9WaXNpYmlsaXR5VHlwZXMSCgoGUFVCTElDEAASCwoHUFJJVkFURRABEgsKB05FVFdPUksQAg==');
@$core.Deprecated('Use loginProvidersEnumDescriptor instead')
const LoginProvidersEnum$json = const {
  '1': 'LoginProvidersEnum',
  '2': const [
    const {'1': 'SMS', '2': 0},
    const {'1': 'EMAIL', '2': 1},
    const {'1': 'FACEBOOK', '2': 2},
    const {'1': 'GOOGLE', '2': 3},
  ],
};

/// Descriptor for `LoginProvidersEnum`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List loginProvidersEnumDescriptor = $convert.base64Decode('ChJMb2dpblByb3ZpZGVyc0VudW0SBwoDU01TEAASCQoFRU1BSUwQARIMCghGQUNFQk9PSxACEgoKBkdPT0dMRRAD');
@$core.Deprecated('Use sendOtpRequestDescriptor instead')
const SendOtpRequest$json = const {
  '1': 'SendOtpRequest',
  '2': const [
    const {'1': 'phone', '3': 1, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
  ],
};

/// Descriptor for `SendOtpRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendOtpRequestDescriptor = $convert.base64Decode('Cg5TZW5kT3RwUmVxdWVzdBIUCgVwaG9uZRgBIAEoCVIFcGhvbmUSFAoFZW1haWwYAiABKAlSBWVtYWls');
@$core.Deprecated('Use sendOtpResponseDescriptor instead')
const SendOtpResponse$json = const {
  '1': 'SendOtpResponse',
  '2': const [
    const {'1': 'request_id', '3': 1, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'limit', '3': 2, '4': 1, '5': 5, '10': 'limit'},
  ],
};

/// Descriptor for `SendOtpResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendOtpResponseDescriptor = $convert.base64Decode('Cg9TZW5kT3RwUmVzcG9uc2USHQoKcmVxdWVzdF9pZBgBIAEoCVIJcmVxdWVzdElkEhQKBWxpbWl0GAIgASgFUgVsaW1pdA==');
@$core.Deprecated('Use verifyOtpRequestDescriptor instead')
const VerifyOtpRequest$json = const {
  '1': 'VerifyOtpRequest',
  '2': const [
    const {'1': 'request_id', '3': 1, '4': 1, '5': 9, '10': 'requestId'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `VerifyOtpRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyOtpRequestDescriptor = $convert.base64Decode('ChBWZXJpZnlPdHBSZXF1ZXN0Eh0KCnJlcXVlc3RfaWQYASABKAlSCXJlcXVlc3RJZBIaCghwYXNzd29yZBgCIAEoCVIIcGFzc3dvcmQ=');
@$core.Deprecated('Use verifyOtpResponseDescriptor instead')
const VerifyOtpResponse$json = const {
  '1': 'VerifyOtpResponse',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 11, '6': '.social.account.Token', '10': 'token'},
    const {'1': 'account', '3': 2, '4': 1, '5': 11, '6': '.social.account.Account', '10': 'account'},
    const {'1': 'action', '3': 3, '4': 1, '5': 9, '10': 'action'},
    const {'1': 'network', '3': 4, '4': 1, '5': 11, '6': '.social.account.Network', '10': 'network'},
  ],
};

/// Descriptor for `VerifyOtpResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verifyOtpResponseDescriptor = $convert.base64Decode('ChFWZXJpZnlPdHBSZXNwb25zZRIrCgV0b2tlbhgBIAEoCzIVLnNvY2lhbC5hY2NvdW50LlRva2VuUgV0b2tlbhIxCgdhY2NvdW50GAIgASgLMhcuc29jaWFsLmFjY291bnQuQWNjb3VudFIHYWNjb3VudBIWCgZhY3Rpb24YAyABKAlSBmFjdGlvbhIxCgduZXR3b3JrGAQgASgLMhcuc29jaWFsLmFjY291bnQuTmV0d29ya1IHbmV0d29yaw==');
@$core.Deprecated('Use accountDescriptor instead')
const Account$json = const {
  '1': 'Account',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'network_id', '3': 2, '4': 1, '5': 9, '10': 'networkId'},
    const {'1': 'username', '3': 3, '4': 1, '5': 9, '10': 'username'},
    const {'1': 'display_name', '3': 4, '4': 1, '5': 9, '10': 'displayName'},
    const {'1': 'phone', '3': 5, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'email', '3': 6, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'password', '3': 7, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'gender', '3': 8, '4': 1, '5': 14, '6': '.social.account.GenderTypes', '10': 'gender'},
    const {'1': 'bio', '3': 9, '4': 1, '5': 9, '10': 'bio'},
    const {'1': 'website_url', '3': 10, '4': 1, '5': 9, '10': 'websiteUrl'},
    const {'1': 'avatar_url', '3': 11, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'birthday', '3': 12, '4': 1, '5': 3, '10': 'birthday'},
    const {'1': 'created_at', '3': 13, '4': 1, '5': 3, '10': 'createdAt'},
    const {'1': 'disabled_at', '3': 14, '4': 1, '5': 3, '10': 'disabledAt'},
    const {'1': 'deleted_at', '3': 15, '4': 1, '5': 3, '10': 'deletedAt'},
    const {'1': 'banned_at', '3': 16, '4': 1, '5': 3, '10': 'bannedAt'},
    const {'1': 'visibility', '3': 17, '4': 1, '5': 14, '6': '.social.account.VisibilityTypes', '10': 'visibility'},
    const {'1': 'is_blocker', '3': 18, '4': 1, '5': 8, '10': 'isBlocker'},
    const {'1': 'is_blocking', '3': 19, '4': 1, '5': 8, '10': 'isBlocking'},
    const {'1': 'stat', '3': 20, '4': 1, '5': 11, '6': '.social.account.AccountStat', '10': 'stat'},
    const {'1': 'is_verified', '3': 21, '4': 1, '5': 8, '10': 'isVerified'},
    const {'1': 'business', '3': 22, '4': 1, '5': 11, '6': '.social.account.BusinessInformation', '10': 'business'},
    const {'1': 'notification', '3': 23, '4': 1, '5': 11, '6': '.social.account.Notification', '10': 'notification'},
    const {'1': 'has_avatar', '3': 24, '4': 1, '5': 8, '10': 'hasAvatar'},
    const {'1': 'role', '3': 25, '4': 1, '5': 9, '10': 'role'},
    const {'1': 'referral_code', '3': 26, '4': 1, '5': 9, '10': 'referralCode'},
    const {'1': 'updated_at', '3': 27, '4': 1, '5': 5, '10': 'updatedAt'},
  ],
};

/// Descriptor for `Account`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List accountDescriptor = $convert.base64Decode('CgdBY2NvdW50Eg4KAmlkGAEgASgJUgJpZBIdCgpuZXR3b3JrX2lkGAIgASgJUgluZXR3b3JrSWQSGgoIdXNlcm5hbWUYAyABKAlSCHVzZXJuYW1lEiEKDGRpc3BsYXlfbmFtZRgEIAEoCVILZGlzcGxheU5hbWUSFAoFcGhvbmUYBSABKAlSBXBob25lEhQKBWVtYWlsGAYgASgJUgVlbWFpbBIaCghwYXNzd29yZBgHIAEoCVIIcGFzc3dvcmQSMwoGZ2VuZGVyGAggASgOMhsuc29jaWFsLmFjY291bnQuR2VuZGVyVHlwZXNSBmdlbmRlchIQCgNiaW8YCSABKAlSA2JpbxIfCgt3ZWJzaXRlX3VybBgKIAEoCVIKd2Vic2l0ZVVybBIdCgphdmF0YXJfdXJsGAsgASgJUglhdmF0YXJVcmwSGgoIYmlydGhkYXkYDCABKANSCGJpcnRoZGF5Eh0KCmNyZWF0ZWRfYXQYDSABKANSCWNyZWF0ZWRBdBIfCgtkaXNhYmxlZF9hdBgOIAEoA1IKZGlzYWJsZWRBdBIdCgpkZWxldGVkX2F0GA8gASgDUglkZWxldGVkQXQSGwoJYmFubmVkX2F0GBAgASgDUghiYW5uZWRBdBI/Cgp2aXNpYmlsaXR5GBEgASgOMh8uc29jaWFsLmFjY291bnQuVmlzaWJpbGl0eVR5cGVzUgp2aXNpYmlsaXR5Eh0KCmlzX2Jsb2NrZXIYEiABKAhSCWlzQmxvY2tlchIfCgtpc19ibG9ja2luZxgTIAEoCFIKaXNCbG9ja2luZxIvCgRzdGF0GBQgASgLMhsuc29jaWFsLmFjY291bnQuQWNjb3VudFN0YXRSBHN0YXQSHwoLaXNfdmVyaWZpZWQYFSABKAhSCmlzVmVyaWZpZWQSPwoIYnVzaW5lc3MYFiABKAsyIy5zb2NpYWwuYWNjb3VudC5CdXNpbmVzc0luZm9ybWF0aW9uUghidXNpbmVzcxJACgxub3RpZmljYXRpb24YFyABKAsyHC5zb2NpYWwuYWNjb3VudC5Ob3RpZmljYXRpb25SDG5vdGlmaWNhdGlvbhIdCgpoYXNfYXZhdGFyGBggASgIUgloYXNBdmF0YXISEgoEcm9sZRgZIAEoCVIEcm9sZRIjCg1yZWZlcnJhbF9jb2RlGBogASgJUgxyZWZlcnJhbENvZGUSHQoKdXBkYXRlZF9hdBgbIAEoBVIJdXBkYXRlZEF0');
@$core.Deprecated('Use businessInformationDescriptor instead')
const BusinessInformation$json = const {
  '1': 'BusinessInformation',
  '2': const [
    const {'1': 'phone', '3': 1, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'category', '3': 3, '4': 1, '5': 9, '10': 'category'},
    const {'1': 'is_active', '3': 4, '4': 1, '5': 8, '10': 'isActive'},
  ],
};

/// Descriptor for `BusinessInformation`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List businessInformationDescriptor = $convert.base64Decode('ChNCdXNpbmVzc0luZm9ybWF0aW9uEhQKBXBob25lGAEgASgJUgVwaG9uZRIUCgVlbWFpbBgCIAEoCVIFZW1haWwSGgoIY2F0ZWdvcnkYAyABKAlSCGNhdGVnb3J5EhsKCWlzX2FjdGl2ZRgEIAEoCFIIaXNBY3RpdmU=');
@$core.Deprecated('Use accountStatDescriptor instead')
const AccountStat$json = const {
  '1': 'AccountStat',
  '2': const [
    const {'1': 'post_count', '3': 1, '4': 1, '5': 3, '10': 'postCount'},
    const {'1': 'follower_count', '3': 2, '4': 1, '5': 3, '10': 'followerCount'},
    const {'1': 'following_count', '3': 3, '4': 1, '5': 3, '10': 'followingCount'},
    const {'1': 'is_follower', '3': 4, '4': 1, '5': 5, '10': 'isFollower'},
    const {'1': 'is_following', '3': 5, '4': 1, '5': 5, '10': 'isFollowing'},
    const {'1': 'has_story', '3': 6, '4': 1, '5': 8, '10': 'hasStory'},
    const {'1': 'is_seen_all_stories', '3': 7, '4': 1, '5': 8, '10': 'isSeenAllStories'},
  ],
};

/// Descriptor for `AccountStat`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List accountStatDescriptor = $convert.base64Decode('CgtBY2NvdW50U3RhdBIdCgpwb3N0X2NvdW50GAEgASgDUglwb3N0Q291bnQSJQoOZm9sbG93ZXJfY291bnQYAiABKANSDWZvbGxvd2VyQ291bnQSJwoPZm9sbG93aW5nX2NvdW50GAMgASgDUg5mb2xsb3dpbmdDb3VudBIfCgtpc19mb2xsb3dlchgEIAEoBVIKaXNGb2xsb3dlchIhCgxpc19mb2xsb3dpbmcYBSABKAVSC2lzRm9sbG93aW5nEhsKCWhhc19zdG9yeRgGIAEoCFIIaGFzU3RvcnkSLQoTaXNfc2Vlbl9hbGxfc3RvcmllcxgHIAEoCFIQaXNTZWVuQWxsU3Rvcmllcw==');
@$core.Deprecated('Use notificationDescriptor instead')
const Notification$json = const {
  '1': 'Notification',
  '2': const [
    const {'1': 'on', '3': 1, '4': 1, '5': 8, '10': 'on'},
    const {'1': 'like', '3': 2, '4': 1, '5': 5, '10': 'like'},
    const {'1': 'follow', '3': 3, '4': 1, '5': 5, '10': 'follow'},
    const {'1': 'accept_follow', '3': 4, '4': 1, '5': 5, '10': 'acceptFollow'},
    const {'1': 'comment', '3': 5, '4': 1, '5': 5, '10': 'comment'},
    const {'1': 'comment_like', '3': 6, '4': 1, '5': 5, '10': 'commentLike'},
  ],
};

/// Descriptor for `Notification`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List notificationDescriptor = $convert.base64Decode('CgxOb3RpZmljYXRpb24SDgoCb24YASABKAhSAm9uEhIKBGxpa2UYAiABKAVSBGxpa2USFgoGZm9sbG93GAMgASgFUgZmb2xsb3cSIwoNYWNjZXB0X2ZvbGxvdxgEIAEoBVIMYWNjZXB0Rm9sbG93EhgKB2NvbW1lbnQYBSABKAVSB2NvbW1lbnQSIQoMY29tbWVudF9saWtlGAYgASgFUgtjb21tZW50TGlrZQ==');
@$core.Deprecated('Use tokenDescriptor instead')
const Token$json = const {
  '1': 'Token',
  '2': const [
    const {'1': 'access_token', '3': 1, '4': 1, '5': 9, '10': 'accessToken'},
    const {'1': 'refresh_token', '3': 2, '4': 1, '5': 9, '10': 'refreshToken'},
  ],
};

/// Descriptor for `Token`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tokenDescriptor = $convert.base64Decode('CgVUb2tlbhIhCgxhY2Nlc3NfdG9rZW4YASABKAlSC2FjY2Vzc1Rva2VuEiMKDXJlZnJlc2hfdG9rZW4YAiABKAlSDHJlZnJlc2hUb2tlbg==');
@$core.Deprecated('Use networkDescriptor instead')
const Network$json = const {
  '1': 'Network',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'username', '3': 3, '4': 1, '5': 9, '10': 'username'},
    const {'1': 'description', '3': 4, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'website_url', '3': 5, '4': 1, '5': 9, '10': 'websiteUrl'},
    const {'1': 'settings', '3': 6, '4': 1, '5': 11, '6': '.social.account.Network.Settings', '10': 'settings'},
    const {'1': 'has_avatar', '3': 7, '4': 1, '5': 8, '10': 'hasAvatar'},
    const {'1': 'avatar_url', '3': 8, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'title_fa', '3': 9, '4': 1, '5': 9, '10': 'titleFa'},
  ],
  '3': const [Network_Settings$json],
};

@$core.Deprecated('Use networkDescriptor instead')
const Network_Settings$json = const {
  '1': 'Settings',
  '2': const [
    const {'1': 'is_active', '3': 1, '4': 1, '5': 8, '10': 'isActive'},
    const {'1': 'is_signup_active', '3': 2, '4': 1, '5': 8, '10': 'isSignupActive'},
    const {'1': 'is_public_access_active', '3': 3, '4': 1, '5': 8, '10': 'isPublicAccessActive'},
    const {'1': 'login_providers', '3': 4, '4': 3, '5': 11, '6': '.social.account.Network.Settings.NetworkLoginProvider', '10': 'loginProviders'},
  ],
  '3': const [Network_Settings_NetworkLoginProvider$json],
};

@$core.Deprecated('Use networkDescriptor instead')
const Network_Settings_NetworkLoginProvider$json = const {
  '1': 'NetworkLoginProvider',
  '2': const [
    const {'1': 'provider', '3': 1, '4': 1, '5': 14, '6': '.social.account.LoginProvidersEnum', '10': 'provider'},
    const {'1': 'config', '3': 2, '4': 1, '5': 9, '10': 'config'},
    const {'1': 'is_active', '3': 3, '4': 1, '5': 8, '10': 'isActive'},
  ],
};

/// Descriptor for `Network`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List networkDescriptor = $convert.base64Decode('CgdOZXR3b3JrEg4KAmlkGAEgASgJUgJpZBIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSGgoIdXNlcm5hbWUYAyABKAlSCHVzZXJuYW1lEiAKC2Rlc2NyaXB0aW9uGAQgASgJUgtkZXNjcmlwdGlvbhIfCgt3ZWJzaXRlX3VybBgFIAEoCVIKd2Vic2l0ZVVybBI8CghzZXR0aW5ncxgGIAEoCzIgLnNvY2lhbC5hY2NvdW50Lk5ldHdvcmsuU2V0dGluZ3NSCHNldHRpbmdzEh0KCmhhc19hdmF0YXIYByABKAhSCWhhc0F2YXRhchIdCgphdmF0YXJfdXJsGAggASgJUglhdmF0YXJVcmwSGQoIdGl0bGVfZmEYCSABKAlSB3RpdGxlRmEa9gIKCFNldHRpbmdzEhsKCWlzX2FjdGl2ZRgBIAEoCFIIaXNBY3RpdmUSKAoQaXNfc2lnbnVwX2FjdGl2ZRgCIAEoCFIOaXNTaWdudXBBY3RpdmUSNQoXaXNfcHVibGljX2FjY2Vzc19hY3RpdmUYAyABKAhSFGlzUHVibGljQWNjZXNzQWN0aXZlEl4KD2xvZ2luX3Byb3ZpZGVycxgEIAMoCzI1LnNvY2lhbC5hY2NvdW50Lk5ldHdvcmsuU2V0dGluZ3MuTmV0d29ya0xvZ2luUHJvdmlkZXJSDmxvZ2luUHJvdmlkZXJzGosBChROZXR3b3JrTG9naW5Qcm92aWRlchI+Cghwcm92aWRlchgBIAEoDjIiLnNvY2lhbC5hY2NvdW50LkxvZ2luUHJvdmlkZXJzRW51bVIIcHJvdmlkZXISFgoGY29uZmlnGAIgASgJUgZjb25maWcSGwoJaXNfYWN0aXZlGAMgASgIUghpc0FjdGl2ZQ==');
@$core.Deprecated('Use emptyDescriptor instead')
const Empty$json = const {
  '1': 'Empty',
};

/// Descriptor for `Empty`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyDescriptor = $convert.base64Decode('CgVFbXB0eQ==');
