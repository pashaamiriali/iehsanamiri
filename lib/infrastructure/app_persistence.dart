import 'package:shared_preferences/shared_preferences.dart';

class AppPersistence {
  final SharedPreferences _instance;

  AppPersistence(this._instance);

  static Future<AppPersistence> getInstance() async {
    var instance = await SharedPreferences.getInstance();
    // instance.clear();
    return AppPersistence(instance);
  }

  Future saveAccessToken(String token) async {
    await _instance.setString('accessToken', token);
  }

  String getAccessToken() {
    return _instance.getString('accessToken') ?? '';
  }

  Future saveRefreshToken(String token) async {
    await _instance.setString('refreshToken', token);
  }

  String getRefreshToken() {
    return _instance.getString('refreshToken') ?? '';
  }

  Future saveUserId(String userId) async {
    await _instance.setString('userId', userId);
  }

  String getUserId() {
    return _instance.getString('userId') ?? '';
  }

  Future saveLogin(bool isLogin) async {
    await _instance.setBool('isLogin', isLogin);
  }

  bool getLogin() {
    return _instance.getBool('isLogin') ?? false;
  }

  logout() async {
    await _instance.remove(accessTokenKey);
    await _instance.remove(refreshTokenKey);
    await _instance.remove(userIdKey);
    await _instance.remove(isLoginKey);
  }
  login(String userId, String accessToken, String refreshToken) async {
    await saveLogin(true);
    await saveUserId(userId);
    await saveAccessToken(accessToken);
    await saveRefreshToken(refreshToken);
  }
}

const String accessTokenKey = 'accessToken';
const String refreshTokenKey = 'refreshToken';
const String userIdKey = 'userId';
const String isLoginKey = 'isLogin';
