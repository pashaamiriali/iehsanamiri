import 'dart:convert';
import 'dart:io';

import 'package:grpc/grpc.dart';
import 'package:protobuf/protobuf.dart';

import 'api/services/account/account.pbgrpc.dart';
import 'certificate.dart';

class AppClient {
  late final ClientChannel clientChannel;
  late final AccountServiceClient accountClient;

  AppClient() {
    clientChannel = _initiateClientChannel();
    accountClient = AccountServiceClient(clientChannel,
        options:
            CallOptions(metadata: {'network_id': '62c3cdc35fca15eef4eb5991'}));
  }

  void dispose() {
    clientChannel.shutdown();
  }

  ClientChannel _initiateClientChannel() {
    return ClientChannel(
      'account.svc.tahlildev.com',
      port: 443,
      options: ChannelOptions(
        credentials: ChannelCredentials.secure(
          certificates: certificate,
          onBadCertificate: (X509Certificate cert, String host) => true,
        ),
        idleTimeout: const Duration(seconds: 3),
        connectionTimeout: const Duration(seconds: 10),
      ),
    );
  }

  List<int>? _certificate;

  List<int> get certificate {
    _certificate ??= utf8.encode(caCert);
    return _certificate!;
  }
}

extension RpcCall<R extends GeneratedMessage, Q extends GeneratedMessage>
on GrpcMethod<R, Q> {
  Future<R> rpcCall(Q? data, {String? accessToken}) {
    Map<String, String> metadata = {'network_id': '62c3cdc35fca15eef4eb5991'};
    if (accessToken != null) {
      metadata['access_token'] = accessToken;
    }
    return this(data ?? (Empty() as Q),
        options: CallOptions(
            timeout: const Duration(seconds: 10), metadata: metadata));
  }
}

typedef GrpcMethod<R extends GeneratedMessage, Q extends GeneratedMessage>
= ResponseFuture<R> Function(Q data, {CallOptions? options});
